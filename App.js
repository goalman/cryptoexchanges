import { createAppContainer } from 'react-navigation'
import { URL, URLSearchParams } from 'whatwg-url'
import { Buffer } from 'buffer'
import { Provider as PaperProvider } from 'react-native-paper'
import 'rxjs'
import React from 'react'
import BottomNavigator from './src/containers/BottomTabNavigator'

// react-native 0.59 add own global URLSearchParams without implementation
// https://github.com/facebook/react-native/blob/e6057095adfdc77ccbbff1c97b1e86b06dae340b/Libraries/Blob/URL.js#L66
global.Buffer = Buffer
global.URL = URL
global.URLSearchParams = URLSearchParams

const Navigator = createAppContainer(BottomNavigator)

export default class App extends React.Component {
  render() {
    return (
      <PaperProvider>
        <Navigator />
      </PaperProvider>
    )
  }
}
