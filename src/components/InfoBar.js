// @flow
import {
  Text, View, TouchableOpacity
} from 'react-native'
import React from 'react'
import Icon from 'react-native-vector-icons/MaterialIcons'
import infoBarStyle from '../themes/infoBar'

type Props = {
  subject: string,
  message: string,
  reason: string
}
class InfoBar extends React.Component<Props> {
  constructor(props) {
    super(props)
    this.state = {
      show: true
    }
  }

  _hide = () => {
    this.setState((prevState) => ({ show: !prevState }))
  }

  _infoRow = (subject, message, reason) => {
    console.log(reason)
    if (this.state.show) {
      return (
        <TouchableOpacity onPress={() => this._hide()}>
          <View style={reason === 'success' ? infoBarStyle.successBar : infoBarStyle.errorBar}>
            <Text style={infoBarStyle.text}>
              {subject}
:
              {' '}
              {message}
            </Text>
            <Icon
              name="close"
              color="white"
              size={20}
              style={infoBarStyle.closeIcon}
            />
          </View>
          <View style={infoBarStyle.divider} />
        </TouchableOpacity>
      )
    }
    return null
  }

  render() {
    const { subject, message, reason } = this.props
    return (
      <View>{this._infoRow(subject, message, reason)}</View>
    )
  }
}


export default InfoBar
