import type ApiBinanceBalance from '../model/types/apiBinanceBalance'
import type ApiBalance from '../model/types/apiBalance'

// binance returns all currencies on exchange
/**
 * Filter Binance balances
 * @param {Array<ApiBinanceBalance>} balances
 * @param {Array<string>} currencies
 * @returns {Array<ApiBinanceBalance>}
 */
export const filterBinanceBalances = (
  balances: Array<ApiBinanceBalance>,
  currencies: Array<string>
): Array<ApiBinanceBalance> => (
  balances.filter((balance) => currencies.some((currency) => currency === balance.asset))
)

/**
 * Map Binance balances
 * @param {Array<ApiBinanceBalance>} balances
 * @returns {Array<ApiBalance>}
 */
export const mapBinanceBalances = (balances: Array<ApiBinanceBalance>): Array<ApiBalance> => (
  balances.map((balance) => {
    if (balance.asset === 'BCC') balance.asset = 'BCH'
    return {
      symbol: balance.asset,
      balance: Number(balance.free) + Number(balance.locked),
      available: Number(balance.free)
    }
  })
)

// TODO sort by CZK
export const sortBinanceBalances = (balances) => balances.sort((a, b) => a.balance.toString().localeCompare(b.balance.toString()))

/**
 * Get binance address url search params
 * @returns {URLSearchParams}
 */
export const getBinanceBalancesParams = (): URLSearchParams => {
  const params = new global.URLSearchParams()
  params.append('recvWindow', 10000)
  return params
}

/**
 * Get binance address url search params
 * @param {string} currencySymbol
 * @returns {URLSearchParams}
 */
export const getBinanceAddressParams = (currencySymbol: string): URLSearchParams => {
  const params = new global.URLSearchParams()
  params.append('asset', currencySymbol)
  params.append('recvWindow', 10000)
  return params
}

/**
 * Get binance withdraw url search params
 * @param {string} currencySymbol
 * @param {string} amount
 * @param {string} address
 * @returns {URLSearchParams}
 */
export const getBinanceWithdrawParams = (currencySymbol: string, amount: number, address: string,): URLSearchParams => {
  const params = new global.URLSearchParams()
  params.append('asset', currencySymbol)
  params.append('amount', amount)
  params.append('address', address)
  params.append('recvWindow', 10000)
  return params
}
