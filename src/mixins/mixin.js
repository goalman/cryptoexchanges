import type { State } from 'react-native/Libraries/Components/ScrollResponder'
import orderBy from 'lodash/orderBy'
import config from '../services/config'
import type ApisBalancesWithErrorsAndCzkRates from '../model/types/apisBalancesWithErrorsAndCzkRates'
import type AllBalancesWithErrors from '../model/types/allBalancesWithErrors'
import type AllBalancesWithErrorsAndTotalCzkBalance from '../model/types/allBalancesWithErrorsAndTotalCzkBalance'
import type BalanceWithCzkPrice from '../model/types/balanceWithCzkPrice'
import type ApiBalance from '../model/types/apiBalance'
import type BinanceBalanceAndAddress from '../model/types/binanceBalanceAndAddress'
import type BinanceBalanceAndAddressWithErrors from '../model/types/binanceBalanceAndAddressWithErrors'

/**
 * Set state
 * @param {InstanceType} componentInstance of component instance
 * @param {any} state to set
 * @returns {Promise<any>}
 */
export const setStateAsync = (componentInstance: InstanceType, state: State): Promise<any> => {
  new Promise((resolve: any): Promise<any> => {
    componentInstance.setState(state, resolve)
  })
}

/**
 * Sum all balances
 * @param {object} allBalances
 * @returns {ApisBalancesWithErrorsAndCzkRates}
 */
export const sumBalances = (allBalances: ApisBalancesWithErrorsAndCzkRates): ApisBalancesWithErrorsAndCzkRates => {
  // array for sum balances
  const zeroBalances: BalanceWithCzkPrice = config.cryptoCompare.currencies.map((currency) => ({
    symbol: currency.symbol,
    name: currency.name,
    balance: 0,
    available: 0,
    czkBalance: 0,
  }))
  // object for balances and errors
  const allBalancesWithErrors: ApisBalancesWithErrorsAndCzkRates = {
    balances: zeroBalances,
    errors: [],
    czkRates: allBalances.czkRates
  }
  // add balances and errors into 'allBalancesWithErrors' object
  Object.entries(allBalances).forEach(([exchange: string, balances: Array<ApiBalance>]) => {
    // check if array doesn't contain czkRates or error
    if (exchange !== 'czkRates') {
      // prevent error
      if (balances.length === config.cryptoCompare.currencies.length) {
        balances.forEach((balance) => {
          allBalancesWithErrors.balances.find((zeroBalance) => zeroBalance.symbol === balance.symbol).balance += Number(balance.balance)
          allBalancesWithErrors.balances.find((zeroBalance) => zeroBalance.symbol === balance.symbol).available += Number(balance.available)
        })
      } else {
        allBalancesWithErrors.errors.push({ [exchange]: balances })
      }
    } else if (Object.keys(allBalances.czkRates).length !== config.cryptoCompare.currencies.length) {
      allBalancesWithErrors.errors.push({ [exchange]: balances })
    }
  })
  return allBalancesWithErrors
}

/**
 * Count CZK detail prices
 * @param balancesAndAddresses
 * @returns {BinanceBalanceAndAddress}
 */
export const countCzkDetailPrices = (balancesAndAddresses: BinanceBalanceAndAddress): BinanceBalanceAndAddressWithErrors => {
  const errors = []
  const exchangeBalances = ['binanceBalance']
  let errorCryptocompare = false

  if (typeof balancesAndAddresses.czkRates !== 'object') {
    errorCryptocompare = true
    errors.push({ CryptoCompare: balancesAndAddresses.czkRates })
  }
  exchangeBalances.forEach((exchangeBalance) => {
    if (balancesAndAddresses[exchangeBalance].balance !== undefined) {
      Object.assign(balancesAndAddresses[exchangeBalance], {
        name: getCurrencyName(balancesAndAddresses[exchangeBalance].symbol),
        czkBalance: errorCryptocompare
          ? ''
          : balancesAndAddresses[exchangeBalance].balance
          / balancesAndAddresses.czkRates[balancesAndAddresses[exchangeBalance].symbol]
      })
    } else {
      errors.push({ [exchangeBalance]: balancesAndAddresses[exchangeBalance] })
    }
  })

  Object.assign(balancesAndAddresses, { errors })
  return balancesAndAddresses
}

/**
 * Count czk prices
 * @param {ApisBalancesWithErrorsAndCzkRates} allBalances to count czk prices
 * @returns {AllBalancesWithErrors} with czk prices
 */
export const countCzkPrices = (allBalances: ApisBalancesWithErrorsAndCzkRates): AllBalancesWithErrors => {
  allBalances.balances.forEach((balance) => {
    Object.assign(balance, { czkBalance: balance.balance / allBalances.czkRates[balance.symbol] })
  })
  // delete 'czkRates' property from 'allBalances'
  delete allBalances.czkRates

  return allBalances
}

/**
 * Count total czk balance
 * @param {AllBalancesWithErrors} allBalances
 * @returns {AllBalancesWithErrorsAndTotalCzkBalance}
 */
export const countTotalCzkBalance = (allBalances: AllBalancesWithErrors): AllBalancesWithErrorsAndTotalCzkBalance => {
  // set new property 'totalCzkBalance' to object 'allBalances'
  const AllBalancesWithTotalBalance = Object.assign(allBalances, { totalCzkBalance: 0 })
  // sum ap all czk balances
  AllBalancesWithTotalBalance.balances.forEach((balance) => {
    AllBalancesWithTotalBalance.totalCzkBalance += balance.czkBalance
  })
  return AllBalancesWithTotalBalance
}

/**
 * Sort all balances
 * @param {AllBalancesWithErrorsAndTotalCzkBalance} allBalances
 * @returns {AllBalancesWithErrorsAndTotalCzkBalance} sorted balances
 */
export const sortAllBalances = (allBalances: AllBalancesWithErrorsAndTotalCzkBalance): AllBalancesWithErrorsAndTotalCzkBalance => (
  Object.assign(allBalances,
    { balances: orderBy(allBalances.balances, ['czkBalance', 'balances'], ['desc']) })
)

/**
 * Get currency name
 * @param {string} symbol
 * @returns {string | undefined} name
 */
export const getCurrencyName = (symbol: string): string | undefined => {
  const currency = config.cryptoCompare.currencies.find((currency) => currency.symbol === symbol)
  if (currency) {
    return currency.name
  }
  return undefined
}
