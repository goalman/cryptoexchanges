const numberRegex = /^\d+([,\.]\d*)?$/

const bitcoinAddressRegex = /'^(\s|^|\:)[1|3][a-km-zA-HJ-NP-Z0-9]{26,34}(\s|$|\.)$/
const litecoinAddressRegex = /^[LM3][a-km-zA-HJ-NP-Z1-9]{26,33}$/
const tronAddressRegex = /^[a-zA-Z0-9]{34}$/
const rippleAddressRegex = /^$/
const ethereumAddressRegex = /^0x[a-fA-F0-9]{40}$/
const bitcoinCashAddressRegex = /^$/
const eosAddressRegex = /^$/
const cardanoAddressRegex = /^$/
const neoAddressRegex = /^$/
const tetherAddressRegex = /^$/

export const validDecimalNumber = (number: string | number): boolean => numberRegex.test(number.toString())

export const numberIsSmallerOrEqual = (number: string | number, compareNumber: string | number) => Number(number) <= Number(compareNumber)

export const numberIsBigger = (number: string | number, compareNumber: string | number) => Number(number) > Number(compareNumber)

export const validCurrencyAddress = (address: string, currency: string) => {
  switch (currency) {
  case 'BTC': return bitcoinAddressRegex.test(address)
  case 'LTC': return litecoinAddressRegex.test(address)
  case 'TRX': return tronAddressRegex.test(address)
  case 'XRP': return rippleAddressRegex.test(address)
  case 'ETH': return ethereumAddressRegex.test(address)
  case 'BCH': return bitcoinCashAddressRegex.test(address)
  case 'EOS': return eosAddressRegex.test(address)
  case 'ADA': return cardanoAddressRegex.test(address)
  case 'NEO': return neoAddressRegex.test(address)
  case 'USDT': return tetherAddressRegex.test(address)
  }
}
