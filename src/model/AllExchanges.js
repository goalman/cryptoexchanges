// @flow
import {catchError, map} from 'rxjs/operators'
import {forkJoin, from, of} from 'rxjs'
import binanceBalanceService from '../services/binance/balance'
import {
  filterBinanceBalances, getBinanceAddressParams, mapBinanceBalances} from '../mixins/binance'
import config from '../services/config'
import {countCzkDetailPrices, countCzkPrices, countTotalCzkBalance, sortAllBalances, sumBalances} from '../mixins/mixin'
import Cryptocompare from './Cryptocompare'
import type AllBalancesWithErrorsAndTotalCzkBalance from './types/allBalancesWithErrorsAndTotalCzkBalance'
import Binance from './Binance'

export default class AllExchanges {
  static getBalances(): Promise<AllBalancesWithErrorsAndTotalCzkBalance> {
    return forkJoin({
      binanceBalances: from(Binance.getApiBalances()).pipe(
        map((response: any) => filterBinanceBalances(response.data.balances, config.binance.currencies)),
        map((balances) => mapBinanceBalances(balances)),
        catchError((error) => of(error))
      ),
      czkRates: from(Cryptocompare.getCzkRates(config.cryptoCompare.currencies)).pipe(
        map((response: any) => response.data),
        catchError((error) => of(error))
      )
    }).pipe(
      map((allBalances) => sumBalances(allBalances)),
      map((allBalances) => countCzkPrices(allBalances)),
      map((allBalances) => countTotalCzkBalance(allBalances)),
      map((allBalances) => sortAllBalances(allBalances)),
    ).toPromise()
  }

  static getCurrencyBalancesAndAddresses(currencySymbol: String, currencyName: String): Promise<AllBalancesWithErrorsAndTotalCzkBalance> {
    return forkJoin({
      binanceBalance: from(Binance.getApiBalances()).pipe(
        map((response: any) => filterBinanceBalances(response.data.balances, [currencySymbol])),
        map((balances) => mapBinanceBalances(balances)),
        map((balances) => balances[0]),
        catchError((error) => of(error))
      ),
      czkRates: from(Cryptocompare.getCzkRates([{name: currencyName, symbol: currencySymbol}])).pipe(
        map((response: any) => response.data),
        catchError((error) => of(error))
      ),
      binanceAddress: from(binanceBalanceService.getDepositAddress(getBinanceAddressParams(currencySymbol))).pipe(
        map((response: any) => response.data.address),
        catchError((error) => of(error))
      )
    }).pipe(
      map((balancesAndAddresses) => countCzkDetailPrices(balancesAndAddresses))
    ).toPromise()
  }
}

