// @flow
import cryptoCompareService from '../services/cryptoCompare/prices'

class Cryptocompare {
  static getCzkRates(currencies: Array<String>): Promise<Array<any>> {
    return cryptoCompareService.getCzkRates(currencies)
  }
}

export default Cryptocompare

