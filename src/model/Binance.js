// @flow
import type IExchange from './interfaces/IExchange'
import {catchError, map} from 'rxjs/operators'
import {from, of} from 'rxjs'
import binanceBalanceService from '../services/binance/balance'
import {filterBinanceBalances, getBinanceBalancesParams, mapBinanceBalances} from '../mixins/binance'
import config from '../services/config'
import type AllBalancesWithErrors from './types/allBalancesWithErrors'
import {AxiosResponse} from 'axios'

export default class Binance implements IExchange {
  static getApiBalances(): Promise<AxiosResponse> {
    return binanceBalanceService.getBalances(getBinanceBalancesParams())
  }

  static getBalances(): Promise<AllBalancesWithErrors> {
    return from(Binance.getApiBalances()).pipe(
      map((response: any) => filterBinanceBalances(response.data.balances, config.binance.currencies)),
      map((balances) => mapBinanceBalances(balances)),
      catchError((error) => of(error))
    ).toPromise()
  }

  static getBalance(currencySymbol: String): Promise<AllBalancesWithErrors> {
    return from(Binance.getApiBalances()).pipe(
      map((response: any) => filterBinanceBalances(response.data.balances, [currencySymbol])),
      map((balances) => mapBinanceBalances(balances)),
      catchError((error) => of(error))
    ).toPromise()
  }
}

