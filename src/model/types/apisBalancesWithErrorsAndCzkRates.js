// @flow
import type BalanceWithCzkPrice from './balanceWithCzkPrice'

interface ApisBalancesWithErrorsAndCzkRates {
  balances: Array<BalanceWithCzkPrice>,
  czkRates: Array<Object>,
  errors: Array<any>
}

export default ApisBalancesWithErrorsAndCzkRates
