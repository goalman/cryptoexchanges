// @flow
import type BalanceWithCzkPrice from './balanceWithCzkPrice'

interface AllBalancesWithErrorsAndTotalCzkBalance {
  balances: Array<BalanceWithCzkPrice>,
  totalCzkBalance: number,
  errors: Array<any>
}

export default AllBalancesWithErrorsAndTotalCzkBalance
