// @flow
interface ConfigCurrency {
  name: string,
  symbol: string,
}

export default ConfigCurrency
