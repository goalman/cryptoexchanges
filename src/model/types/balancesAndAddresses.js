import type ApiBalance from './apiBalance'

interface BalancesAndAddresses {
  binanceBalance: ApiBalance,
  czkRates: Object,
  binanceAddress: string,
}

export default BalancesAndAddresses
