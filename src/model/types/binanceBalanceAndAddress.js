// @flow
import type ApiBalance from './apiBalance'

interface BinanceBalanceAndAddress {
  binanceBalance: ApiBalance,
  czkRates: Object,
  binanceAddress: string,
}

export default BinanceBalanceAndAddress
