// @flow
interface BalanceWithCzkPrice {
  symbol: string,
  name: string,
  balance: number,
  available: number,
  czkBalance: number,
}

export default BalanceWithCzkPrice
