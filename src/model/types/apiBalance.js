// @flow
interface ApiBalance {
  symbol: string,
  balance: number,
  available: number,
}

export default ApiBalance
