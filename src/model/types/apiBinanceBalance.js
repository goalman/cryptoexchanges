// @flow
interface ApiBinanceBalance {
  asset: string,
  free: number,
  locked: number
}

export default ApiBinanceBalance
