// @flow
import type BalanceWithCzkPrice from './BalanceWithCzkPrice'

interface BinanceBalanceAndAddressWithErrors {
  binanceBalance: BalanceWithCzkPrice,
  czkRates: Object,
  binanceAddress: string,
  errors: Array<any>
}

export default BinanceBalanceAndAddressWithErrors
