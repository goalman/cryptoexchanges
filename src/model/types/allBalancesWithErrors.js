// @flow
import type BalanceWithCzkPrice from './balanceWithCzkPrice'

interface AllBalancesWithErrors {
  balances: Array<BalanceWithCzkPrice>,
  errors: Array<any>
}

export default AllBalancesWithErrors
