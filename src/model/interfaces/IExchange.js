// @flow
import type BalanceWithCzkPrice from '../types/balanceWithCzkPrice'
import {AxiosResponse} from 'axios'

interface IExchange {
  getApiBalances(): Promise<AxiosResponse>,
  getBalances(): BalanceWithCzkPrice
}

export default IExchange
