import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 1,
    position: 'absolute'
  },
  successBar: {
    width: '100%',
    paddingVertical: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(0,219,0,0.85)',
    color: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  errorBar: {
    width: '100%',
    paddingVertical: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(243,0,13,0.87)',
    color: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  text: {
    width: '90%',
    color: 'white'
  },
  divider: {
    height: 1,
    backgroundColor: 'white'
  },
  closeIcon: {
    width: '10%',
  }
})
