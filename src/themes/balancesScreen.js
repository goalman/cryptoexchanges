import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
    flex: 1,
  },

  item: {
    alignItems: 'flex-start',
    fontSize: 20,
    color: 'black'
  },

  header: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 25,
    marginTop: '5%'
  },

  totalCzkBalance: {
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 25,
    marginTop: '3%',
    marginBottom: '4%'
  },

  dataTable: {
    height: '100%',
    paddingLeft: '2%',
    paddingRight: '2%'
  },

  dataTableTitleCell: {
    fontSize: 14,
    fontWeight: 'normal'
  },

  dataTableTitleNameCell: {
    fontWeight: 'bold',
    fontSize: 14
  },

  dataTableRow: {
    fontWeight: 'bold',
    height: '6.36%'
  },

  dataTableNameCell: {
    fontWeight: 'bold',
    fontSize: 16
  },

  dataTableCell: {
    fontSize: 16
  }
})
