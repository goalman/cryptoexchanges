import { StyleSheet, Platform, StatusBar } from 'react-native'

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight + 15

export default StyleSheet.create({
  statusBar: {
    color: 'black',
    height: STATUSBAR_HEIGHT
  }
})
