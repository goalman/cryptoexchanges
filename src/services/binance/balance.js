import { AxiosResponse } from 'axios'
import httpClient from '../httpClient'
import config from '../config'

const { request } = httpClient

// Nam        Type  Mandatory
// recvWindow LONG  NO
// timestamp  LONG  YES
/**
 * Get balances
 * @param {URLSearchParams} params
 * @returns {Promise<any>}
 */
function getBalances(params: URLSearchParams): Promise<AxiosResponse> {
  return request({
    url: '/api/v3/account',
    method: 'GET',
    baseURL: config.binance.url,
    params,
  })
}

// https://github.com/binance-exchange/binance-official-api-docs/blob/master/wapi-api.md
/**
 * Get Binance deposit address of the currency
 * @param {URLSearchParams} params
 * @returns {Promise<any>}
 */
function getDepositAddress(params: URLSearchParams): Promise<AxiosResponse> {
  return request({
    url: '/wapi/v3/depositAddress.html',
    method: 'GET',
    baseURL: config.binance.url,
    params,
  })
}

// https://github.com/binance-exchange/binance-official-api-docs/blob/master/wapi-api.md
/**
 * Withdraw Binance balance
 * @param {URLSearchParams} params
 * @returns {Promise<any>}
 */
function withdrawBalance(params: URLSearchParams): Promise<AxiosResponse> {
  return request({
    url: '/wapi/v3/withdraw.html',
    method: 'POST',
    baseURL: config.binance.url,
    params,
  })
}

export default {
  getBalances,
  getDepositAddress,
  withdrawBalance
}
