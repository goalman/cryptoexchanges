import request from '../httpClient'
import config from '../config'

function getUserDataStream() {
  request.post('/api/userDataStream', {
    exchange: 'binance',
    apiVersion: 'v1',
    security: {
      apiKey: true,
      secretKey: true
    }
  })
}

// Name       Type    Mandatory
// listenKey  STRING  YES
function keepStreamAlive(params) {
  request.put('/api/v1/userDataStream', {
    baseURL: config.binance.url,
    headers: {
      'X-MBX-APIKEY': config.binance.apiKey,
      'X-MBX-SECRET': config.binance.secretKey
    },
    params
  })
}

// Name       Type    Mandatory
// listenKey  STRING  YES
function closeStream(params) {
  request.delete('/api/v1/userDataStream', {
    baseURL: config.binance.url,
    headers: {
      'X-MBX-APIKEY': config.binance.apiKey,
      'X-MBX-SECRET': config.binance.secretKey
    },
    params
  })
}

export default (
  getUserDataStream,
  keepStreamAlive,
  closeStream
)
