import Axios from 'axios'
import Crypto from 'crypto-js'
import config from './config'

// Create an instance using the config defaults provided by the library
// At this point the timeout config value is `0` as is the default for the library
const client = Axios.create()

client.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
client.defaults.timeout = 2500

// Add a request interceptor
client.interceptors.request.use((request) => {
  if (!request.params) {
    request.params = new global.URLSearchParams()
  }
  if (request.baseURL === config.binance.url) {
    request.params.append('timestamp', Date.now().toString())
    request.params.append('signature', Crypto.HmacSHA256(request.params.toString(), config.binance.secretKey))
    request.headers = { 'X-MBX-APIKEY': config.binance.apiKey }
  }
  if (request.baseURL === config.cryptoCompare.url) {
    request.headers = { authorization: `Apikey${config.cryptoCompare.apiKey}` }
  }
  // console.log(request)
  return request
})

client.interceptors.response.use((response) =>
  // Do something with response data
  // console.log(response) // eslint-disable-line
  response,
(error) => {
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    console.log(error.response.data) // eslint-disable-line
    console.log(error.response.status) // eslint-disable-line
    console.log(error.response.headers) // eslint-disable-line
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    // http.ClientRequest in node.js
    console.log(error.request) // eslint-disable-line
  } else {
    // Something happened in setting up the request that triggered an Error
    console.log('Error', error.message) // eslint-disable-line
  }
  console.log(error.config) // eslint-disable-line
  return Promise.reject(error)
})

const request = (options) => client.request(options)

export default {
  client,
  request
}
