export default {
  exchanges: ['Binance'],
  binance: {
    url: 'https://api.binance.com',
    balanceRequestTimeout: 10000,
    apiKey: 'rleG4Vhrf6UDWzkjXL3soe9XVWea9USYPYxjDbmvTSyPjx99vGKf0dH5G8PbNBVy',
    secretKey: 'y1i6cos8kyESdF6V5lK2KRXqgnVQpBHPVmddgfbjgogaFrXQadE8IvBqxWntWQpM',
    currencies: ['BTC', 'ETH', 'XRP', 'BCC', 'EOS', 'ADA', 'LTC', 'TRX', 'USDT', 'NEO']
  },

  // coinMarketCap: {
  //   url: 'https://pro-api.coinmarketcap.com',
  //   apiKey: '33cb1ee6-2b5c-40dc-a6fb-5fba6fc4c4b9',
  //   currencies: [
  //     {
  //       name: 'Bitcoin',
  //       symbol: 'BTC'
  //     },
  //     {
  //       name: 'Ethereum',
  //       symbol: 'ETH'
  //     },
  //     {
  //       name: 'Ripple',
  //       symbol: 'XRP'
  //     },
  //     {
  //       name: 'Bitcoin Cash',
  //       symbol: 'BCH'
  //     },
  //     {
  //       name: 'EOS',
  //       symbol: 'EOS'
  //     },
  //     {
  //       name: 'Cardano',
  //       symbol: 'ADA'
  //     },
  //     {
  //       name: 'Litecoin',
  //       symbol: 'LTC'
  //     },
  //     {
  //       name: 'Tron',
  //       symbol: 'TRX'
  //     },
  //     {
  //       name: 'Tether',
  //       symbol: 'USDT'
  //     },
  //     {
  //       name: 'NEO',
  //       symbol: 'NEO'
  //     }
  //   ]
  // },

  cryptoCompare: {
    url: 'https://min-api.cryptocompare.com',
    apiKey: 'c62131a0aa0dbff855e78d0fb4bee96cba65cfcf066ae9fef8f2135ca50bdec4',
    currencies: [
      {
        name: 'Bitcoin',
        symbol: 'BTC'
      },
      {
        name: 'Ethereum',
        symbol: 'ETH'
      },
      {
        name: 'Ripple',
        symbol: 'XRP'
      },
      {
        name: 'Bitcoin Cash',
        symbol: 'BCH'
      },
      {
        name: 'EOS',
        symbol: 'EOS'
      },
      {
        name: 'Cardano',
        symbol: 'ADA'
      },
      {
        name: 'Litecoin',
        symbol: 'LTC'
      },
      {
        name: 'Tron',
        symbol: 'TRX'
      },
      {
        name: 'Tether',
        symbol: 'USDT'
      },
      {
        name: 'NEO',
        symbol: 'NEO'
      }
    ]
  },

  euroCentralBank: {
    url: 'https://api.exchangeratesapi.io'
  }
}
