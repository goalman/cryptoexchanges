import httpClient from '../httpClient'
import config from '../config'
import type ConfigCurrency from '../../model/types/configCurrency'
import {getCryptocomaparePricesParams} from './utils'

const { request } = httpClient

// https://min-api.cryptocompare.com/documentation
function getPrices(params) {
  return request({
    url: '/data/price',
    method: 'GET',
    baseURL: config.cryptoCompare.url,
    params
  })
}

function getCzkRates(currencies: Array<ConfigCurrency>) {
  return getPrices(getCryptocomaparePricesParams(currencies))
}

export default {
  getPrices,
  getCzkRates
}
