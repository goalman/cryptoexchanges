import type ConfigCurrency from '../../model/types/configCurrency'

/**
 * Get Cryptocompare prices params
 * @param {Array<ConfigCurrency>} currencies with name and symbol
 * @returns {URLSearchParams}
 */
export function getCryptocomaparePricesParams (currencies: Array<ConfigCurrency>): URLSearchParams {
  const params = new global.URLSearchParams()
  params.append('fsym', 'CZK')
  params.append('tsyms', getTsymsParamValue(currencies))
  return params
}

/**
 * Get Cryptocompare Tsyms param value
 * @param {Array<ConfigCurrency>} currencies with name and symbol
 * @returns {string} with currencies symbols
 */
function getTsymsParamValue (currencies: Array<ConfigCurrency>): string {
  let string = ''
  currencies.forEach((currency) => {
    string += `${currency.symbol},`
  })
  return string
}
