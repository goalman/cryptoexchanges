import { createStackNavigator } from 'react-navigation'
import BalanceActionsScreen from './screens/BalanceDetailScreen'
import BalancesScreen from './screens/BalancesScreen'

const BalanceStack = createStackNavigator({
  Balances: {
    screen: BalancesScreen,
    navigationOptions: {
      header: null
    },
  },
  Actions: {
    screen: BalanceActionsScreen,
    navigationOptions: ({ navigation }) => ({
      title: navigation.state.params.currencyName,
    }),
  },
})

export default BalanceStack
