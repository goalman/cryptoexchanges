// @flow
import {
  Text, View, SafeAreaView, RefreshControl, Platform, ProgressBarAndroid, ProgressViewIOS, ScrollView, Button
} from 'react-native'
import React from 'react'
import { DataTable } from 'react-native-paper'
import styles from '../../themes/index'
import AllExchanges from '../../model/AllExchanges'
import balancesStyle from '../../themes/balancesScreen'
import TopStatusBar from '../../components/TopStatusBar'
import InfoBar from '../../components/InfoBar'
import { roundNumber, numberFormatToShow } from '../../mixins/filters'
import type AllBalancesWithErrorsAndTotalCzkBalance from '../../model/types/allBalancesWithErrorsAndTotalCzkBalance'
import type BalanceWithCzkPrice from '../../model/types/balanceWithCzkPrice'
import infoBarStyle from '../../themes/infoBar'
import config from '../../services/config'

type State = {
  refreshing: boolean,
  allBalances: AllBalancesWithErrorsAndTotalCzkBalance
}

export default class BalancesScreen extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    this.state = {
      refreshing: false,
      allBalances: {}
    }
  }

  async componentDidMount() {
    this.loadBalances()
  }

  loadBalances = () => {
    this.setState(() => ({ refreshing: true }))
    AllExchanges.getBalances().then((balancesData) => {
      console.log(balancesData)
      this.setState(() => ({ allBalances: balancesData }))
    }).finally(() => {
      this.setState(() => ({refreshing: false}))
    })
  }

  render(): React$Node {
    const { totalCzkBalance, balances, errors } = this.state.allBalances
    const { navigate } = this.props.navigation
    return (
      <SafeAreaView style={balancesStyle.container}>
        <ScrollView
          contentContainerStyle={{ flex: 1 }}
          refreshControl={(
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.loadBalances}
            />
          )}
        >
          <TopStatusBar backgroundColor="#772ea2" barStyle="light-content" />
          <View>
            <Error errors={errors} />
            <Text style={balancesStyle.header}>Prostředky</Text>
            <TotalBalance balances={balances} totalCzkBalance={totalCzkBalance} />
            <DataTable style={balancesStyle.dataTable}>
              <DataTable.Header>
                <DataTable.Title>
                  <Text
                    onPress={() => {
                      alert('You tapped the button!')
                    }}
                    style={balancesStyle.dataTableTitleNameCell}
                  >
Název
                  </Text>
                </DataTable.Title>
                <DataTable.Title numeric><Text style={balancesStyle.dataTableTitleCell}>Počet ks</Text></DataTable.Title>
                <DataTable.Title numeric><Text style={balancesStyle.dataTableTitleCell}>Hodnota Kč</Text></DataTable.Title>
              </DataTable.Header>
              <TableRows balances={balances} navigate={navigate} />
              <DataTable.Pagination style={styles.invisible} />
            </DataTable>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const TableRows = (props: AllBalancesWithErrorsAndTotalCzkBalance): React$Element => {
  const { balances, navigate } = props
  if (balances) {
    return balances.map((balance: BalanceWithCzkPrice) => (
      <DataTable.Row
        key={balance.symbol}
        style={balancesStyle.dataTableRow}
        onPress={() => navigate('Actions', { currencySymbol: balance.symbol, currencyName: balance.name })}
      >
        <DataTable.Cell><Text style={balancesStyle.dataTableNameCell}>{balance.name}</Text></DataTable.Cell>
        <DataTable.Cell numeric><Text style={balancesStyle.dataTableCell}>{numberFormatToShow(balance.balance)}</Text></DataTable.Cell>
        <DataTable.Cell numeric><Text style={balancesStyle.dataTableCell}>{numberFormatToShow(roundNumber(balance.czkBalance, 2), 8)}</Text></DataTable.Cell>
      </DataTable.Row>
    ))
  }
  return null
}

const TotalBalance = (props: AllBalancesWithErrorsAndTotalCzkBalance) => {
  const { balances } = props
  const { totalCzkBalance } = props
  if (balances) {
    return (
      <Text style={balancesStyle.totalCzkBalance}>
Celkem
        {'    '}
        {numberFormatToShow(roundNumber(totalCzkBalance, 2))}
        {' '}
Kč
      </Text>
    )
  }
  return null
}

// const ProgressBar = (props: Object) => {
//   if (!props.balances) {
//     if (Platform.OS === 'ios') return <ProgressViewIOS />
//     return <ProgressBarAndroid />
//   }
//   return null
// }

const getErrorsTexts = (errors): Array<Object> => {
  const errorsTexts = []
  errors.forEach((error) => {
    const foundExchange = config.exchanges.find((exchange) => Object.keys(error)[0].includes(exchange.toLowerCase()))
    const subject = foundExchange || 'Cryptocompare'
    let message = Object.keys(error)[0].includes('Balance')
      ? 'Zůstatek kryptoměn se nepodařilo načíst.'
      : 'Aktuální kurz kryptoměny se nepodařilo načíst.'
    message += ' Potažením dolů opakujte pokus.'
    errorsTexts.push({ [subject]: message })
  })
  return errorsTexts
}

const Error = (props) => {
  const { errors } = props
  if (errors && errors.length) {
    return (
      <View style={infoBarStyle.container}>
        {getErrorsTexts(errors).map((error: any) => (
          <InfoBar
            subject={Object.keys(error)[0]}
            message={Object.values(error)[0]}
            reason="error"
            key={Math.random().toString()}
          />
        ))}
      </View>
    )
  }
  return null
}
