// @flow
import {
  Platform, ProgressBarAndroid, ProgressViewIOS, RefreshControl, SafeAreaView, TextInput, ScrollView, Text, View,
  TouchableOpacity, Picker, Alert
} from 'react-native'
import React from 'react'
import { NavigationScreenProp } from 'react-navigation'
import { DataTable } from 'react-native-paper'
import TopStatusBar from '../../components/TopStatusBar'
import styles from '../../themes/index'
import balanceDetailStyle from '../../themes/balanceDetailScreen'
import infoBarStyle from '../../themes/infoBar'
import InfoBar from '../../components/InfoBar'
import {
  decimalPointToComma, decimalCommaToPoint, numberFormatToShow, roundNumber
} from '../../mixins/filters'
import {
  validDecimalNumber, numberIsSmallerOrEqual, validCurrencyAddress, numberIsBigger
} from '../../mixins/rules'
import { getBinanceWithdrawParams } from '../../mixins/binance'
import config from '../../services/config'
import binanceBalanceService from '../../services/binance/balance'
import AllExchanges from '../../model/AllExchanges'
import type BalancesAndAddresses from '../../model/types/balancesAndAddresses'

type Props = {
  navigation: NavigationScreenProp,
  loading: boolean
}

type State = {
  refreshing: boolean,
  balancesAndAddresses: BalancesAndAddresses,
  exchangeSelectedFrom: string,
  exchangeSelectedTo: string,
  amountToSend: boolean,
  maxAmountToSend: number,
  withdrawAddress: string,
  amountToSendErrorText: string,
  withdrawAddressErrorText: string,
  errors: Array<any>,
  successes: []
}

export default class BalanceDetailScreen extends React.Component<Props, State> {
  constructor(props) {
    super(props)
    this.state = {
      refreshing: false,
      balancesAndAddresses: {},
      exchangeSelectedFrom: '',
      exchangeSelectedTo: '',
      amountToSend: '',
      maxAmountToSend: 0,
      withdrawAddress: '',
      amountToSendErrorText: '',
      withdrawAddressErrorText: '',
      errors: [],
      successes: []
    }
  }

  async componentDidMount(): void {
    await this.loadBalancesAndAddresses()
    this.setDefaultData()
  }

  loadBalancesAndAddresses = ():Promise<any> => {
    this.setState(() => ({ refreshing: true }))
    const { currencySymbol, currencyName } = this.props.navigation.state.params
    return AllExchanges.getCurrencyBalancesAndAddresses(currencySymbol, currencyName).then((data) => {
      this.setState(() => ({balancesAndAddresses: data}))
    }).finally(() => {
      this.setState(() => ({ refreshing: false }))
    })
  }

  setDefaultData(): void {
    if (this.state.exchangeSelectedFrom === '') {
      this.setState(() => ({ exchangeSelectedFrom: 'Binance' }))
      const maxAmountToSend = decimalPointToComma(this.state.balancesAndAddresses.binanceBalance.available)
      this.setState(() => ({ maxAmountToSend }))
    }
    if (this.state.exchangeSelectedTo === '') {
      this.setState(() => ({ exchangeSelectedTo: 'Vlastní adresa' }))
    }
    this.setState(() => ({ refreshing: false }))
  }

  withdrawBinanceBalance(): void {
    const { currencySymbol, currencyName } = this.props.navigation.state.params
    this.setState(() => ({ refreshing: true }))
    binanceBalanceService.withdrawBalance(getBinanceWithdrawParams(
      currencySymbol,
      this.state.amountToSend,
      this.state.withdrawAddress
    ))
      .then((response) => {
        this.setState(() => ({ amountToSend: '' }))
        const binanceSuccessText = { Binance: `Zůstatek byl odeslán na ${this.state.exchangeSelectedTo}.` }
        this.setState(() => ({ successes: [...this.state.successes, binanceSuccessText] }))
        this.loadBalancesAndAddresses()
      })
      .catch((error) => {
        const binanceErrorText = { Binance: 'Zůstatek se nepodařilo odeslat.' }
        this.setState(() => ({ errors: [...this.state.errors, binanceErrorText] }))
      })
      .finally(() => {
        this.setState(() => ({ refreshing: false }))
      })
  }

  withdrawBalance(): void {
    if (this.state.exchangeSelectedFrom === 'Binance') {
      this.withdrawBinanceBalance()
    }
  }

  checkFormValues(): boolean {
    const { currencySymbol, currencyName } = this.props.navigation.state.params
    const errors = []
    if (!validDecimalNumber(decimalCommaToPoint(this.state.amountToSend))) {
      errors.push(false)
      this.setState(() => ({ amountToSendErrorText: 'Zadejte číslo s desetinou čárkou nebo tečkou.' }))
    } else if (!numberIsSmallerOrEqual(
      decimalCommaToPoint(this.state.amountToSend),
      decimalCommaToPoint(this.state.maxAmountToSend)
    )) {
      errors.push(false)
      this.setState(() => ({ amountToSendErrorText: 'Hodnota nesmí být větší než dostupný zůstatek.' }))
    } else if (!numberIsBigger(
      decimalCommaToPoint(this.state.amountToSend),
      0
    )) {
      errors.push(false)
      this.setState(() => ({ amountToSendErrorText: 'Hodnota musí být větší než nula.' }))
    } else if (this.state.exchangeSelectedFrom === this.state.exchangeSelectedTo) {
      errors.push(false)
      this.setState(() => (
        { amountToSendErrorText: 'Vybrané burzy musí být rozdílné' }
      ))
    } else {
      this.setState(() => ({ amountToSendErrorText: '' }))
    }
    if (!validCurrencyAddress(this.state.withdrawAddress, currencySymbol)) {
      errors.push(false)
      this.setState(() => ({ withdrawAddressErrorText: `Adresa musí mít validní formát kryptoměny ${currencyName}.` }))
    } else {
      this.setState(() => ({ withdrawAddressErrorText: '' }))
    }
    return !errors.length
  }

  setExchangeSelectedFrom(selectedExchange: string): void {
    const { currencySymbol } = this.props.navigation.state.params
    this.setState(() => ({ exchangeSelectedFrom: selectedExchange }))
    const maxAmountToSend = decimalPointToComma(
      this.state.balancesAndAddresses[`${selectedExchange.toLowerCase()}Balance`].available
    )
    this.setState(() => ({ maxAmountToSend }))
  }

  setExchangeSelectedTo(selectedExchange: string): void {
    this.setState(() => ({ exchangeSelectedTo: selectedExchange }))
    const withdrawAddress = selectedExchange !== 'Vlastní adresa'
      ? this.state.balancesAndAddresses[`${selectedExchange.toLowerCase()}Address`]
      : ''
    this.setState(() => ({ withdrawAddress }))
  }

  onButtonSendClick(): React$Node {
    const { currencySymbol } = this.props.navigation.state.params
    if (this.checkFormValues()) {
      Alert.alert(
        'Opravdu chcete odeslat',
        `${this.state.amountToSend} ${currencySymbol}  na ${this.state.exchangeSelectedTo}
        \nAdresa: \n${this.state.withdrawAddress}`,
        [
          {
            text: 'Zavřít',
            style: 'cancel'
          },
          { text: 'Odeslat', onPress: () => this.withdrawBalance() }
        ],
        { cancelable: true }
      )
    }
  }

  render(): React$Node {
    const {
      binanceBalance, errors
    } = this.state.balancesAndAddresses
    return (
      <SafeAreaView style={balanceDetailStyle.container}>
        <ScrollView
          contentContainerStyle={{ flex: 1 }}
          refreshControl={(
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this.loadBalancesAndAddresses}
            />
          )}
        >
          <TopStatusBar backgroundColor="#772ea2" barStyle="light-content" />
          <View>
            <Error errors={errors} />
            <Success successes={this.state.successes} />
            <DataTable style={balanceDetailStyle.dataTable}>
              <DataTable.Header>
                <DataTable.Title><Text style={balanceDetailStyle.dataTableTitleNameCell}>Burza</Text></DataTable.Title>
                <DataTable.Title numeric><Text style={balanceDetailStyle.dataTableTitleCell}>Dostupný</Text></DataTable.Title>
                <DataTable.Title numeric><Text style={balanceDetailStyle.dataTableTitleCell}>Celkem</Text></DataTable.Title>
                <DataTable.Title numeric><Text style={balanceDetailStyle.dataTableTitleCell}>Celkem v Kč </Text></DataTable.Title>
              </DataTable.Header>
              <TableRows
                binanceBalance={binanceBalance}
              />
              <DataTable.Pagination style={styles.invisible} />
            </DataTable>
          </View>
          <View style={balanceDetailStyle.withdrawContainer}>
            <View style={balanceDetailStyle.headBox}>
              <Text style={balanceDetailStyle.headBoxLabelText}>Z burzy:</Text>
              <Picker
                selectedValue={this.state.exchangeSelectedFrom}
                style={balanceDetailStyle.picker}
                onValueChange={(selectedValue) => this.setExchangeSelectedFrom(selectedValue)}
              >
                {PickerValuesExchangeFrom()}
              </Picker>
            </View>
            <View style={balanceDetailStyle.inputBoxAmount}>
              <Text>Množství:</Text>
              <TextInput
                style={balanceDetailStyle.inputAmountValue}
                placeholder="0,00"
                onChangeText={(value) => this.setState(() => ({ amountToSend: value }))}
                value={this.state.amountToSend.toString()}
                keyboardType="numeric"
              />
              <Text>
/
                {this.state.maxAmountToSend}
              </Text>
              <Text style={balanceDetailStyle.inputErrorText}>{this.state.amountToSendErrorText}</Text>
            </View>
            <View style={balanceDetailStyle.headBox}>
              <Text style={balanceDetailStyle.headBoxLabelText}>Na burzu (adresu):</Text>
              <Picker
                selectedValue={this.state.exchangeSelectedTo}
                style={balanceDetailStyle.picker}
                onValueChange={(selectedValue) => this.setExchangeSelectedTo(selectedValue)}
              >
                {PickerValuesExchangeTo()}
              </Picker>
            </View>
            <View style={balanceDetailStyle.inputBoxAddress}>
              <TextInput
                placeholder="1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2"
                onChangeText={(address) => this.setState(() => ({ withdrawAddress: address }))}
                editable={this.state.exchangeSelectedTo === 'Vlastní adresa'}
                value={this.state.withdrawAddress}
              />
              <Text style={balanceDetailStyle.inputErrorText}>{this.state.withdrawAddressErrorText}</Text>
            </View>
            <TouchableOpacity
              style={balanceDetailStyle.sendButton}
              onPress={() => {
                this.onButtonSendClick()
              }}
            >
              <Text style={balanceDetailStyle.sendButtonText}>Odeslat</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    )
  }
}

const Success = (props) => {
  const { successes } = props
  if (successes && successes.length) {
    return (
      <View style={infoBarStyle.container}>
        {successes.map((success: Object) => (
          <InfoBar
            subject={Object.keys(success)[0]}
            message={Object.values(success)[0]}
            reason="success"
            key={Math.random().toString()}
          />
        ))}
      </View>
    )
  }
  return null
}

const getErrorsTexts = (errors): Array<Object> => {
  const errorsTexts = []
  errors.forEach((error) => {
    const foundExchange = config.exchanges.find((exchange) => Object.keys(error)[0].includes(exchange.toLowerCase()))
    const subject = foundExchange || 'Cryptocompare'
    let message = Object.keys(error)[0].includes('Balance')
      ? 'Zůstatek kryptoměny se nepodařilo načíst.'
      : Object.keys(error)[0].includes('Address')
        ? 'Adresu pro převod kryptoměny se nepodařilo načíst.'
        : 'Aktuální kurz kryptoměny se nepodařilo načíst.'
    message += ' Potažením dolů opakujte pokus.'
    errorsTexts.push({ [subject]: message })
  })
  return errorsTexts
}

const Error = (props) => {
  const { errors } = props
  if (errors && errors.length) {
    return (
      <View style={infoBarStyle.container}>
        {getErrorsTexts(errors).map((error: any) => (
          <InfoBar
            subject={Object.keys(error)[0]}
            message={Object.values(error)[0]}
            reason="error"
            key={Math.random().toString()}
          />
        ))}
      </View>
    )
  }
  return null
}

const TableRows = (props): React$Element => {
  const {
    binanceBalance
  } = props
  if (binanceBalance) {
    const exchanges = [
      { name: 'Binance', balance: binanceBalance },
    ]
    return exchanges.map((exchange: Object) => (
      <DataTable.Row style={balanceDetailStyle.dataTableRow} key={exchange.name}>
        <DataTable.Cell><Text style={balanceDetailStyle.dataTableNameCell}>{exchange.name}</Text></DataTable.Cell>
        <DataTable.Cell numeric><Text style={balanceDetailStyle.dataTableCell}>{numberFormatToShow(exchange.balance.available)}</Text></DataTable.Cell>
        <DataTable.Cell numeric><Text style={balanceDetailStyle.dataTableCell}>{numberFormatToShow(exchange.balance.balance)}</Text></DataTable.Cell>
        <DataTable.Cell numeric><Text style={balanceDetailStyle.dataTableCell}>{numberFormatToShow(roundNumber(exchange.balance.czkBalance, 2))}</Text></DataTable.Cell>
      </DataTable.Row>
    ))
  }
  return null
}

// const ProgressBar = (props) => {
//   const {
//     binanceBalance
//   } = props
//   if (!binanceBalance) {
//     if (Platform.OS === 'ios') return <ProgressViewIOS />
//     return <ProgressBarAndroid />
//   }
//   return null
// }

const PickerValuesExchangeFrom = () => (
  config.exchanges.map((exchange) => (<Picker.Item label={exchange} value={exchange} key={exchange} />))
)

const PickerValuesExchangeTo = () => {
  const values = [...config.exchanges, 'Vlastní adresa']
  return values.map((exchange) => (<Picker.Item label={exchange} value={exchange} key={exchange} />))
}
