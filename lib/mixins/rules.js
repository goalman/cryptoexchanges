Object.defineProperty(exports,"__esModule",{value:true});exports.validCurrencyAddress=exports.numberIsBigger=exports.numberIsSmallerOrEqual=exports.validDecimalNumber=void 0;var numberRegex=/^\d+([,\.]\d*)?$/;

var bitcoinAddressRegex=/'^(\s|^|\:)[1|3][a-km-zA-HJ-NP-Z0-9]{26,34}(\s|$|\.)$/;
var litecoinAddressRegex=/^[LM3][a-km-zA-HJ-NP-Z1-9]{26,33}$/;
var tronAddressRegex=/^[a-zA-Z0-9]{34}$/;
var rippleAddressRegex=/^$/;
var ethereumAddressRegex=/^0x[a-fA-F0-9]{40}$/;
var bitcoinCashAddressRegex=/^$/;
var eosAddressRegex=/^$/;
var cardanoAddressRegex=/^$/;
var neoAddressRegex=/^$/;
var tetherAddressRegex=/^$/;

var validDecimalNumber=function validDecimalNumber(number){return numberRegex.test(number.toString());};exports.validDecimalNumber=validDecimalNumber;

var numberIsSmallerOrEqual=function numberIsSmallerOrEqual(number,compareNumber){return Number(number)<=Number(compareNumber);};exports.numberIsSmallerOrEqual=numberIsSmallerOrEqual;

var numberIsBigger=function numberIsBigger(number,compareNumber){return Number(number)>Number(compareNumber);};exports.numberIsBigger=numberIsBigger;

var validCurrencyAddress=function validCurrencyAddress(address,currency){
switch(currency){
case'BTC':return bitcoinAddressRegex.test(address);
case'LTC':return litecoinAddressRegex.test(address);
case'TRX':return tronAddressRegex.test(address);
case'XRP':return rippleAddressRegex.test(address);
case'ETH':return ethereumAddressRegex.test(address);
case'BCH':return bitcoinCashAddressRegex.test(address);
case'EOS':return eosAddressRegex.test(address);
case'ADA':return cardanoAddressRegex.test(address);
case'NEO':return neoAddressRegex.test(address);
case'USDT':return tetherAddressRegex.test(address);}

};exports.validCurrencyAddress=validCurrencyAddress;