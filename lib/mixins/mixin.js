var _interopRequireDefault=require("@babel/runtime/helpers/interopRequireDefault");Object.defineProperty(exports,"__esModule",{value:true});exports.getCurrencyName=exports.sortAllBalances=exports.countTotalCzkBalance=exports.countCzkPrices=exports.countCzkDetailPrices=exports.sumBalances=exports.setStateAsync=void 0;var _extends2=_interopRequireDefault(require("@babel/runtime/helpers/extends"));var _defineProperty2=_interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));var _slicedToArray2=_interopRequireDefault(require("@babel/runtime/helpers/slicedToArray"));
var _orderBy=_interopRequireDefault(require("lodash/orderBy"));
var _config=_interopRequireDefault(require("../services/config"));














var setStateAsync=function setStateAsync(componentInstance,state){
new Promise(function(resolve){
componentInstance.setState(state,resolve);
});
};exports.setStateAsync=setStateAsync;






var sumBalances=function sumBalances(allBalances){

var zeroBalances=_config.default.cryptoCompare.currencies.map(function(currency){return{
symbol:currency.symbol,
name:currency.name,
balance:0,
available:0,
czkBalance:0};});


var allBalancesWithErrors={
balances:zeroBalances,
errors:[],
czkRates:allBalances.czkRates};


Object.entries(allBalances).forEach(function(_ref){var _ref2=(0,_slicedToArray2.default)(_ref,2),exchange=_ref2[0],balances=_ref2[1];

if(exchange!=='czkRates'){

if(balances.length===_config.default.cryptoCompare.currencies.length){
balances.forEach(function(balance){
allBalancesWithErrors.balances.find(function(zeroBalance){return zeroBalance.symbol===balance.symbol;}).balance+=Number(balance.balance);
allBalancesWithErrors.balances.find(function(zeroBalance){return zeroBalance.symbol===balance.symbol;}).available+=Number(balance.available);
});
}else{
allBalancesWithErrors.errors.push((0,_defineProperty2.default)({},exchange,balances));
}
}else if(Object.keys(allBalances.czkRates).length!==_config.default.cryptoCompare.currencies.length){
allBalancesWithErrors.errors.push((0,_defineProperty2.default)({},exchange,balances));
}
});
return allBalancesWithErrors;
};exports.sumBalances=sumBalances;






var countCzkDetailPrices=function countCzkDetailPrices(balancesAndAddresses){
var errors=[];
var exchangeBalances=['binanceBalance'];
var errorCryptocompare=false;

if(typeof balancesAndAddresses.czkRates!=='object'){
errorCryptocompare=true;
errors.push({CryptoCompare:balancesAndAddresses.czkRates});
}
exchangeBalances.forEach(function(exchangeBalance){
if(balancesAndAddresses[exchangeBalance].balance!==undefined){
(0,_extends2.default)(balancesAndAddresses[exchangeBalance],{
name:getCurrencyName(balancesAndAddresses[exchangeBalance].symbol),
czkBalance:errorCryptocompare?
'':
balancesAndAddresses[exchangeBalance].balance/
balancesAndAddresses.czkRates[balancesAndAddresses[exchangeBalance].symbol]});

}else{
errors.push((0,_defineProperty2.default)({},exchangeBalance,balancesAndAddresses[exchangeBalance]));
}
});

(0,_extends2.default)(balancesAndAddresses,{errors:errors});
return balancesAndAddresses;
};exports.countCzkDetailPrices=countCzkDetailPrices;






var countCzkPrices=function countCzkPrices(allBalances){
allBalances.balances.forEach(function(balance){
(0,_extends2.default)(balance,{czkBalance:balance.balance/allBalances.czkRates[balance.symbol]});
});

delete allBalances.czkRates;

return allBalances;
};exports.countCzkPrices=countCzkPrices;






var countTotalCzkBalance=function countTotalCzkBalance(allBalances){

var AllBalancesWithTotalBalance=(0,_extends2.default)(allBalances,{totalCzkBalance:0});

AllBalancesWithTotalBalance.balances.forEach(function(balance){
AllBalancesWithTotalBalance.totalCzkBalance+=balance.czkBalance;
});
return AllBalancesWithTotalBalance;
};exports.countTotalCzkBalance=countTotalCzkBalance;






var sortAllBalances=function sortAllBalances(allBalances){return(
(0,_extends2.default)(allBalances,
{balances:(0,_orderBy.default)(allBalances.balances,['czkBalance','balances'],['desc'])}));};exports.sortAllBalances=sortAllBalances;







var getCurrencyName=function getCurrencyName(symbol){
var currency=_config.default.cryptoCompare.currencies.find(function(currency){return currency.symbol===symbol;});
if(currency){
return currency.name;
}
return undefined;
};exports.getCurrencyName=getCurrencyName;