Object.defineProperty(exports,"__esModule",{value:true});exports.getTsymsParamValue=exports.getCryptocomaparePricesParams=void 0;






var getCryptocomaparePricesParams=function getCryptocomaparePricesParams(currencies){
var params=new global.URLSearchParams();
params.append('fsym','CZK');
params.append('tsyms',getTsymsParamValue(currencies));
return params;
};exports.getCryptocomaparePricesParams=getCryptocomaparePricesParams;






var getTsymsParamValue=function getTsymsParamValue(currencies){
var string='';
currencies.forEach(function(currency){
string+=currency.symbol+",";
});
return string;
};exports.getTsymsParamValue=getTsymsParamValue;