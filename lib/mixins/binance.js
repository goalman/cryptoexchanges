Object.defineProperty(exports,"__esModule",{value:true});exports.getBinanceWithdrawParams=exports.getBinanceAddressParams=exports.getBinanceBalancesParams=exports.sortBinanceBalances=exports.mapBinanceBalances=exports.filterBinanceBalances=void 0;









var filterBinanceBalances=function filterBinanceBalances(
balances,
currencies){return(

balances.filter(function(balance){return currencies.some(function(currency){return currency===balance.asset;});}));};exports.filterBinanceBalances=filterBinanceBalances;







var mapBinanceBalances=function mapBinanceBalances(balances){return(
balances.map(function(balance){
if(balance.asset==='BCC')balance.asset='BCH';
return{
symbol:balance.asset,
balance:Number(balance.free)+Number(balance.locked),
available:Number(balance.free)};

}));};exports.mapBinanceBalances=mapBinanceBalances;



var sortBinanceBalances=function sortBinanceBalances(balances){return balances.sort(function(a,b){return a.balance.toString().localeCompare(b.balance.toString());});};exports.sortBinanceBalances=sortBinanceBalances;





var getBinanceBalancesParams=function getBinanceBalancesParams(){
var params=new global.URLSearchParams();
params.append('recvWindow',10000);
return params;
};exports.getBinanceBalancesParams=getBinanceBalancesParams;






var getBinanceAddressParams=function getBinanceAddressParams(currencySymbol){
var params=new global.URLSearchParams();
params.append('asset',currencySymbol);
params.append('recvWindow',10000);
return params;
};exports.getBinanceAddressParams=getBinanceAddressParams;








var getBinanceWithdrawParams=function getBinanceWithdrawParams(currencySymbol,amount,address){
var params=new global.URLSearchParams();
params.append('asset',currencySymbol);
params.append('amount',amount);
params.append('address',address);
params.append('recvWindow',10000);
return params;
};exports.getBinanceWithdrawParams=getBinanceWithdrawParams;