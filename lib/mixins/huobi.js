Object.defineProperty(exports, '__esModule', { value: true }); exports.getHuobiAddressParams = exports.sortHuobiBalances = exports.mapHuobiBalances = exports.filterHuobiBalances = void 0


const filterHuobiBalances = function filterHuobiBalances(balances, currencies) {
  return (

    balances.filter((balance) => (
      currencies.some((currency) => currency === balance.currency.toUpperCase()))))
}; exports.filterHuobiBalances = filterHuobiBalances


const mapHuobiBalances = function mapHuobiBalances(balances, currencies) {
  const zeroBalances = currencies.map((currency) => ({
    symbol: currency,
    balance: 0,
    available: 0
  }))

  zeroBalances.forEach((obj) => {
    balances.forEach((balance) => {
      if (obj.symbol === balance.currency.toUpperCase()) {
        obj.balance += Number(balance.balance)
        obj.available = obj.type === 'trade' ? Number(balance.balance) : obj.available
      }
    })
  })
  return zeroBalances
}; exports.mapHuobiBalances = mapHuobiBalances


const sortHuobiBalances = function sortHuobiBalances(balances) { return balances.sort((a, b) => a.balance.toString().localeCompare(b.balance.toString())) }; exports.sortHuobiBalances = sortHuobiBalances


const getHuobiAddressParams = function getHuobiAddressParams(currencySymbol) {
  const params = new global.URLSearchParams()
  params.append('currency', currencySymbol.toLowerCase())
  return params
}; exports.getHuobiAddressParams = getHuobiAddressParams
