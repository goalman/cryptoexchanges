Object.defineProperty(exports, '__esModule', { value: true }); exports.getBitfinexAddressData = exports.sortBitfinexBalances = exports.mapBitfinexBalances = exports.filterBitfinexBalances = void 0


const filterBitfinexBalances = function filterBitfinexBalances(
  balances,
  currencies
) {
  return (

    balances.filter((balance) => (
      currencies.some((currency) => (
        currency === balance.currency.toUpperCase() && balance.type === 'exchange')))))
}; exports.filterBitfinexBalances = filterBitfinexBalances


const mapBitfinexBalances = function mapBitfinexBalances(balances, currencies) {
  return (
    currencies.map((currency) => {
      const foundBalance = balances.find((balance) => balance.currency.toUpperCase() === currency)
      return {
        symbol: currency,
        balance: foundBalance ? Number(foundBalance.amount) : 0,
        available: foundBalance ? Number(foundBalance.available) : 0
      }
    }))
}; exports.mapBitfinexBalances = mapBitfinexBalances


const sortBitfinexBalances = function sortBitfinexBalances(balances) { return balances.sort((a, b) => a.balance.toString().localeCompare(b.balance.toString())) }; exports.sortBitfinexBalances = sortBitfinexBalances


const getBitfinexAddressData = function getBitfinexAddressData(currencyName) {
  return {
    method: currencyName.toLowerCase(),
    wallet_name: 'deposit',
    renew: 0
  }
}; exports.getBitfinexAddressData = getBitfinexAddressData
