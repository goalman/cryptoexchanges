Object.defineProperty(exports, '__esModule', { value: true }); exports.getBittrexBalanceParams = exports.getBittrexAddressParams = exports.sortBittrexBalances = exports.mapBittrexBalances = void 0


const mapBittrexBalances = function mapBittrexBalances(balances) {
  return (
    balances.map((balance) => ({
      symbol: balance.Currency,
      balance: balance.Balance ? Number(balance.Balance) : 0,
      available: balance.Available ? Number(balance.Available) : 0
    })))
}; exports.mapBittrexBalances = mapBittrexBalances


const sortBittrexBalances = function sortBittrexBalances(balances) { return balances.sort((a, b) => a.balance.toString().localeCompare(b.balance.toString())) }; exports.sortBittrexBalances = sortBittrexBalances


const getBittrexAddressParams = function getBittrexAddressParams(currencySymbol) {
  const params = new global.URLSearchParams()
  params.append('currency', currencySymbol)
  return params
}; exports.getBittrexAddressParams = getBittrexAddressParams


const getBittrexBalanceParams = function getBittrexBalanceParams(currencySymbol) {
  const params = new global.URLSearchParams()
  params.append('currency', currencySymbol)
  return params
}; exports.getBittrexBalanceParams = getBittrexBalanceParams
