Object.defineProperty(exports,"__esModule",{value:true});exports.decimalCommaToPoint=exports.decimalPointToComma=exports.numberFormatToShow=exports.maxStringLength=exports.roundNumber=void 0;






var roundNumber=function roundNumber(number,precision){
var editedPrecision=1;
for(var i=1;i<=precision;i++){
editedPrecision*=10;
}
return Math.round(number*editedPrecision+Number.EPSILON)/editedPrecision;
};exports.roundNumber=roundNumber;






var maxStringLength=function maxStringLength(string,maxChars){return string.substring(0,maxChars);};exports.maxStringLength=maxStringLength;






var numberFormatToShow=function numberFormatToShow(number,maxChars){
if(typeof number==='number'&&!isNaN(number)){
var numberString=decimalPointToComma(number);



















return numberString;
}
return'-';
};exports.numberFormatToShow=numberFormatToShow;





var decimalPointToComma=function decimalPointToComma(number){
if(typeof number==='number'){
var numberString=number.toString();
return numberString.replace('.',',');
}
return null;
};exports.decimalPointToComma=decimalPointToComma;





var decimalCommaToPoint=function decimalCommaToPoint(number){
var numberString=number.toString();
return numberString.replace(',','.');
};exports.decimalCommaToPoint=decimalCommaToPoint;