const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))
const _account = _interopRequireDefault(require('./account'))
const _axios = require('axios')

const

  { request } = _httpClient.default


function getBalances(accountId) {
  return request({
    url: `/v1/account/accounts/${accountId}/balance`,
    method: 'GET',
    baseURL: _config.default.huobi.url
  })
}


function getHuobiAccountBalances() {
  return _account.default.getAccount()
    .then((response) => {
      const accountId = response.data.data.find((account) => account.type === 'spot').id
      return getBalances(accountId)
    })
}


function getDepositAddress(params) {
  return request({
    url: '/v2/account/deposit/address',
    method: 'GET',
    baseURL: _config.default.huobi.url,
    params
  })
}


function withdrawBalance(params) {
  return request({
    url: '/v1/dw/withdraw/api/create',
    method: 'POST',
    baseURL: _config.default.huobi.url,
    params
  })
} const _default = {
  getBalances,
  getHuobiAccountBalances,
  getDepositAddress
}; exports.default = _default
