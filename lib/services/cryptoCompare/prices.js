const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))

const

  { request } = _httpClient.default


function getPrices(params) {
  return request({
    url: '/data/price',
    method: 'GET',
    baseURL: _config.default.cryptoCompare.url,
    params
  })
}

function getSupportedCurrenciesCzkRates() {
  const getTsymsParamValue = function getTsymsParamValue() {
    let string = ''
    _config.default.cryptoCompare.currencies.forEach((currency) => {
      string += `${currency.symbol},`
    })
    return string
  }
  const params = new global.URLSearchParams()
  params.append('fsym', 'CZK')
  params.append('tsyms', getTsymsParamValue())
  return getPrices(params)
} const _default = {
  getPrices,
  getSupportedCurrenciesCzkRates
}; exports.default = _default
