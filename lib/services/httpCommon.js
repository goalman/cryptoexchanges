const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _defineProperty2 = _interopRequireDefault(require('@babel/runtime/helpers/defineProperty')); const _axios = _interopRequireDefault(require('axios'))
const _cryptoJs = _interopRequireDefault(require('crypto-js'))
const _config = _interopRequireDefault(require('./config'))

function ownKeys(object, enumerableOnly) { const keys = Object.keys(object); if (Object.getOwnPropertySymbols) { let symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly)symbols = symbols.filter((sym) => Object.getOwnPropertyDescriptor(object, sym).enumerable); keys.push.apply(keys, symbols) } return keys } function _objectSpread(target) { for (let i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach((key) => { (0, _defineProperty2.default)(target, key, source[key]) }) } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) } else { ownKeys(source).forEach((key) => { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)) }) } } return target }


const client = _axios.default.create()

client.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
client.defaults.timeout = 2500


client.interceptors.request.use((request) => {
  if (!request.params) {
    request.params = new global.URLSearchParams()
  }
  if (request.baseURL === _config.default.binance.url) {
    request.params.append('timestamp', Date.now().toString())
    request.params.append('signature', _cryptoJs.default.HmacSHA256(request.params.toString(), _config.default.binance.secretKey))
    request.headers = { 'X-MBX-APIKEY': _config.default.binance.apiKey }
  }
  if (request.baseURL === _config.default.bittrex.url) {
    request.params.append('apikey', _config.default.bittrex.apiKey)
    request.params.append('nonce', Math.floor(Date.now() / 1000).toString())
    request.headers = {
      apisign: _cryptoJs.default.HmacSHA512(`${
        request.baseURL}${request.url}?${request.params.toString()}`,
      _config.default.bittrex.secretKey)
    }
  }
  if (request.baseURL === _config.default.bitfinex.url) {
    request.data = JSON.stringify(_objectSpread({},
      request.data, {
        request: request.url,
        nonce: (Date.now() * 1000).toString()
      }))

    request.headers = {
      'Content-Type': 'application/json',
      'X-BFX-APIKEY': _config.default.bitfinex.apiKey,
      'X-BFX-PAYLOAD': _cryptoJs.default.enc.Base64.stringify(_cryptoJs.default.enc.Utf8.parse(request.data)),
      'X-BFX-SIGNATURE': _cryptoJs.default.HmacSHA384(_cryptoJs.default.enc.Base64.stringify(_cryptoJs.default.enc.Utf8.parse(request.data)),
        _config.default.bitfinex.secretKey)
    }
  }
  if (request.baseURL === _config.default.huobi.url) {
    request.params.append('AccessKeyId', _config.default.huobi.apiKey)
    request.params.append('SignatureMethod', 'HmacSHA256')
    request.params.append('SignatureVersion', '2')
    request.params.append('Timestamp', new Date().toISOString().substring(0, 19))
    request.params.sort()
    const payload = `${request.method.toUpperCase()}\n${request.baseURL.replace('https://', '')}\n${request.url}\n${request.params.toString()}`
    request.params.append('Signature', _cryptoJs.default.enc.Base64.stringify(_cryptoJs.default.HmacSHA256(payload, _config.default.huobi.secretKey)))
  }
  if (request.baseURL === _config.default.cryptoCompare.url) {
    request.headers = { authorization: `Apikey${_config.default.cryptoCompare.apiKey}` }
  }

  return request
})

client.interceptors.response.use((response) => response, (error) => {
  if (error.response) {
    console.log(error.response.data)
    console.log(error.response.status)
    console.log(error.response.headers)
  } else if (error.request) {
    console.log(error.request)
  } else {
    console.log('Error', error.message)
  }
  console.log(error.config)
  return Promise.reject(error)
})

const request = function request(options) { return client.request(options) }; const _default = {
  client,
  request
}; exports.default = _default
