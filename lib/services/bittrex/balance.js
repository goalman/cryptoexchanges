const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))
const _axios = require('axios')

const

  { request } = _httpClient.default


function getBalances() {
  return request({
    url: '/account/getbalances',
    method: 'GET',
    baseURL: _config.default.bittrex.url
  })
}


function getBalance(params) {
  return request({
    url: '/account/getbalance',
    method: 'GET',
    baseURL: _config.default.bittrex.url,
    params
  })
}


function getSpecificBalances(currencies) {
  const promises = []
  currencies.forEach((currency) => {
    const params = new global.URLSearchParams()
    params.append('currency', currency)
    promises.push(getBalance(params).then((response) => response.data.result))
  })
  return Promise.all(promises)
}


function getDepositAddress(params) {
  return request({
    url: '/account/getdepositaddress',
    method: 'GET',
    baseURL: _config.default.bittrex.url,
    params
  })
}


function withdrawBalance(params) {
  return request({
    url: '/account/withdraw',
    method: 'GET',
    baseURL: _config.default.bittrex.url,
    params
  })
} const _default = {
  getBalances,
  getBalance,
  getSpecificBalances,
  getDepositAddress
}; exports.default = _default
