const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))
const _axios = require('axios')

const

  { request } = _httpClient.default


function getBalances(data) {
  return request({
    url: '/v1/balances',
    method: 'POST',
    baseURL: _config.default.bitfinex.url,
    data
  })
}


function getDepositAddress(data) {
  return request({
    url: '/v1/deposit/new',
    method: 'POST',
    baseURL: _config.default.bitfinex.url,
    data
  })
}


function withdrawBalance(params) {
  return request({
    url: '/v1/withdraw',
    method: 'POST',
    baseURL: _config.default.bitfinex.url,
    params
  })
} const _default = {
  getBalances,
  getDepositAddress,
  withdrawBalance
}; exports.default = _default
