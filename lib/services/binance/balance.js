const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _httpClient = _interopRequireDefault(require('../httpClient'))
const _config = _interopRequireDefault(require('../config'))
const _axios = require('axios')

const

  { request } = _httpClient.default


function getBalances(params) {
  return request({
    url: '/api/v3/account',
    method: 'GET',
    baseURL: _config.default.binance.url,
    params
  })
}


function getDepositAddress(params) {
  return request({
    url: '/wapi/v3/depositAddress.html',
    method: 'GET',
    baseURL: _config.default.binance.url,
    params
  })
}


function withdrawBalance(params) {
  return request({
    url: '/wapi/v3/withdraw.html',
    method: 'POST',
    baseURL: _config.default.binance.url,
    params
  })
} const _default = {
  getBalances,
  getDepositAddress,
  withdrawBalance
}; exports.default = _default
