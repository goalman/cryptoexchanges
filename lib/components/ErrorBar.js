const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _classCallCheck2 = _interopRequireDefault(require('@babel/runtime/helpers/classCallCheck')); const _createClass2 = _interopRequireDefault(require('@babel/runtime/helpers/createClass')); const _possibleConstructorReturn2 = _interopRequireDefault(require('@babel/runtime/helpers/possibleConstructorReturn')); const _getPrototypeOf2 = _interopRequireDefault(require('@babel/runtime/helpers/getPrototypeOf')); const _inherits2 = _interopRequireDefault(require('@babel/runtime/helpers/inherits'))
const _reactNative = require('react-native')


const _react = _interopRequireDefault(require('react'))
const _errorBar = _interopRequireDefault(require('../themes/errorBar'))
const _MaterialIcons = _interopRequireDefault(require('@expo/vector-icons/MaterialIcons'))

const _jsxFileName = 'C:\\Users\\tiboy\\IdeaProjects\\cryptotradition\\src\\components\\ErrorBar.js'; const


  ErrorBar = (function (_React$Component) {
    (0, _inherits2.default)(ErrorBar, _React$Component)
    function ErrorBar(props) {
      let _this; (0, _classCallCheck2.default)(this, ErrorBar)
      _this = (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(ErrorBar).call(this, props)); _this


        ._hide = function () {
          _this.setState((prevState) => ({ show: !prevState }))
        }; _this

        ._errorRow = function (subject, message) {
          if (_this.state.show) {
            return (
              _react.default.createElement(_reactNative.TouchableOpacity, { onPress: function onPress() { _this._hide() }, __source: { fileName: _jsxFileName, lineNumber: 28 } },
                _react.default.createElement(_reactNative.View, { style: _errorBar.default.bar, __source: { fileName: _jsxFileName, lineNumber: 29 } },
                  _react.default.createElement(_reactNative.Text, { style: _errorBar.default.text, __source: { fileName: _jsxFileName, lineNumber: 30 } }, subject, ': ', message),

                  _react.default.createElement(_MaterialIcons.default, {
                    name: 'close',
                    color: 'white',
                    size: 20,
                    style: _errorBar.default.closeIcon,
                    __source: { fileName: _jsxFileName, lineNumber: 32 }
                  })),


                _react.default.createElement(_reactNative.View, { style: _errorBar.default.divider, __source: { fileName: _jsxFileName, lineNumber: 39 } })))
          }
          return null
        }; _this.state = { show: true }; return _this
    }(0, _createClass2.default)(ErrorBar, [{
      key: 'render',
      value: function render() {
        const _this$props = this.props; const { subject } = _this$props; const { message } = _this$props
        return (
          _react.default.createElement(_reactNative.View, { __source: { fileName: _jsxFileName, lineNumber: 50 } }, this._errorRow(subject, message)))
      }
    }]); return ErrorBar
  }(_react.default.Component)); const _default = ErrorBar; exports.default = _default
