var _interopRequireDefault=require("@babel/runtime/helpers/interopRequireDefault");Object.defineProperty(exports,"__esModule",{value:true});exports.default=void 0;var _classCallCheck2=_interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));var _createClass2=_interopRequireDefault(require("@babel/runtime/helpers/createClass"));var _possibleConstructorReturn2=_interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));var _getPrototypeOf2=_interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));var _inherits2=_interopRequireDefault(require("@babel/runtime/helpers/inherits"));
var _reactNative=require("react-native");


var _react=_interopRequireDefault(require("react"));
var _MaterialIcons=_interopRequireDefault(require("@expo/vector-icons/MaterialIcons"));
var _infoBar=_interopRequireDefault(require("../themes/infoBar"));var _jsxFileName="C:\\Users\\tiboy\\IdeaProjects\\cryptoexchanges\\src\\components\\InfoBar.js";var






InfoBar=function(_React$Component){(0,_inherits2.default)(InfoBar,_React$Component);
function InfoBar(props){var _this;(0,_classCallCheck2.default)(this,InfoBar);
_this=(0,_possibleConstructorReturn2.default)(this,(0,_getPrototypeOf2.default)(InfoBar).call(this,props));_this.





_hide=function(){
_this.setState(function(prevState){return{show:!prevState};});
};_this.

_infoRow=function(subject,message,reason){
console.log(reason);
if(_this.state.show){
return(
_react.default.createElement(_reactNative.TouchableOpacity,{onPress:function onPress(){return _this._hide();},__source:{fileName:_jsxFileName,lineNumber:30}},
_react.default.createElement(_reactNative.View,{style:reason==='success'?_infoBar.default.successBar:_infoBar.default.errorBar,__source:{fileName:_jsxFileName,lineNumber:31}},
_react.default.createElement(_reactNative.Text,{style:_infoBar.default.text,__source:{fileName:_jsxFileName,lineNumber:32}},
subject,":",

' ',
message),

_react.default.createElement(_MaterialIcons.default,{
name:"close",
color:"white",
size:20,
style:_infoBar.default.closeIcon,__source:{fileName:_jsxFileName,lineNumber:38}})),


_react.default.createElement(_reactNative.View,{style:_infoBar.default.divider,__source:{fileName:_jsxFileName,lineNumber:45}})));


}
return null;
};_this.state={show:true};return _this;}(0,_createClass2.default)(InfoBar,[{key:"render",value:function render()

{var _this$props=
this.props,subject=_this$props.subject,message=_this$props.message,reason=_this$props.reason;
return(
_react.default.createElement(_reactNative.View,{__source:{fileName:_jsxFileName,lineNumber:55}},this._infoRow(subject,message,reason)));

}}]);return InfoBar;}(_react.default.Component);var _default=



InfoBar;exports.default=_default;