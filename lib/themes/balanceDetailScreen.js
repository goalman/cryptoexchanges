Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _reactNative = require('react-native')

const _default = _reactNative.StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },


  dataTable: {
    height: '50%',
    paddingLeft: '2%',
    paddingRight: '2%'
  },


  dataTableTitleCell: {
    fontSize: 14,
    fontWeight: 'normal'
  },


  dataTableTitleNameCell: {
    fontWeight: 'bold',
    fontSize: 14
  },


  dataTableRow: {
    fontWeight: 'bold',
    height: '43%'
  },


  dataTableNameCell: {
    fontWeight: 'bold',
    fontSize: 16
  },


  dataTableCell: { fontSize: 16 },


  withdrawContainer: {
    flex: 1,
    flexDirection: 'column',
    margin: '4%'
  },


  headBox: {
    width: '100%',
    height: '19%',
    backgroundColor: '#00baff',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 25,
    borderRadius: 3
  },


  headBoxLabelText: {
    marginLeft: '2%',
    color: 'white'
  },


  picker: {
    height: '100%',
    width: '60%',
    fontWeight: '900',
    color: 'white',
    justifyContent: 'center',
    alignItems: 'flex-end',
    textAlign: 'center'
  },


  inputBoxAmount: {
    width: '100%',
    height: '24%',
    flexDirection: 'row',
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#a6a6a6',
    justifyContent: 'space-between',
    alignItems: 'center',
    textAlign: 'left',
    paddingHorizontal: '2%'
  },


  inputBoxAddress: {
    width: '100%',
    height: '24%',
    flexDirection: 'row',
    borderRadius: 3,
    borderWidth: 1,
    borderColor: '#a6a6a6',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center'
  },


  inputAmountValue: {
    fontSize: 16,
    marginRight: '15%'
  },


  inputErrorText: {
    position: 'absolute',
    color: 'red',
    alignSelf: 'flex-end',
    fontSize: 12.5,
    marginLeft: '2%'
  },


  sendButton: {
    height: '24%',
    backgroundColor: '#00baff',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    borderRadius: 3
  },


  sendButtonText: {
    fontWeight: 'bold',
    color: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 20
  }
}); exports.default = _default
