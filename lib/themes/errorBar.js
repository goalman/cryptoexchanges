Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _reactNative = require('react-native')

const _default = _reactNative.StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 1,
    position: 'absolute'
  },

  bar: {
    width: '100%',
    paddingVertical: 10,
    paddingLeft: 20,
    backgroundColor: 'rgba(243,0,13,0.87)',
    color: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  text: {
    width: '90%',
    color: 'white'
  },

  divider: {
    height: 1,
    backgroundColor: 'white'
  },

  closeIcon: { width: '10%' }
}); exports.default = _default
