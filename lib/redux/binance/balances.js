const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.binanceBalancesRequestEpic = exports.binanceBalancesReducer = exports.onBinanceBalancesError = exports.onBinanceBalancesSuccess = exports.onBinanceBalancesRequest = exports.initialState = void 0; const _toConsumableArray2 = _interopRequireDefault(require('@babel/runtime/helpers/toConsumableArray')); const _defineProperty2 = _interopRequireDefault(require('@babel/runtime/helpers/defineProperty')); const _rxjs = require('rxjs')
const _operators = require('rxjs/operators')


const _reduxObservable = require('redux-observable')
const _balance = _interopRequireDefault(require('../../services/binance/balance'))
const _binance = require('../../mixins/binance')

const _this = this; function ownKeys(object, enumerableOnly) { const keys = Object.keys(object); if (Object.getOwnPropertySymbols) { let symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly)symbols = symbols.filter((sym) => Object.getOwnPropertyDescriptor(object, sym).enumerable); keys.push.apply(keys, symbols) } return keys } function _objectSpread(target) { for (let i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach((key) => { (0, _defineProperty2.default)(target, key, source[key]) }) } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) } else { ownKeys(source).forEach((key) => { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)) }) } } return target }

const initialState = {
  binanceBalances: [],
  error: null
}; exports.initialState = initialState


const onBinanceBalancesRequest = function onBinanceBalancesRequest(currencies) {
  return {
    type: 'ON_BINANCE_BALANCES_REQUEST',
    currencies
  }
}; exports.onBinanceBalancesRequest = onBinanceBalancesRequest

const onBinanceBalancesSuccess = function onBinanceBalancesSuccess(binanceBalances) {
  return {
    type: 'ON_BINANCE_BALANCES_SUCCESS',
    binanceBalances
  }
}; exports.onBinanceBalancesSuccess = onBinanceBalancesSuccess

const onBinanceBalancesError = function onBinanceBalancesError(error) {
  return {
    type: 'ON_BINANCE_BALANCES_ERROR',
    error
  }
}; exports.onBinanceBalancesError = onBinanceBalancesError


const binanceBalancesReducer = function binanceBalancesReducer() {
  const state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState; const action = arguments.length > 1 ? arguments[1] : undefined
  switch (action.type) {
  case 'ON_BINANCE_BALANCES_REQUEST':
    return _objectSpread({},
      state, { currencies: action.currencies })

  case 'ON_BINANCE_BALANCES_SUCCESS':
    return _objectSpread({},
      state, { binanceBalances: action.binanceBalances })

  case 'ON_BINANCE_BALANCES_ERROR':
    return _objectSpread({},
      state, { errors: [].concat((0, _toConsumableArray2.default)(_this.errors), [action.error]) })

  default:
    return state
  }
}; exports.binanceBalancesReducer = binanceBalancesReducer

const binanceBalancesRequestEpic = function binanceBalancesRequestEpic(action$) {
  return action$.pipe(
    (0, _reduxObservable.ofType)('ON_BINANCE_BALANCES_REQUEST'),
    (0, _operators.switchMap)(() => (0, _rxjs.from)(_balance.default.getBalances(action$.currencies)).pipe(
      (0, _operators.map)((response) => (0, _binance.filterBinanceBalances)(response.data.balances)),
      (0, _operators.map)((balances) => (0, _binance.mapBinanceBalances)(balances)),
      (0, _operators.flatMap)((response) => (0, _rxjs.from)([onBinanceBalancesSuccess(response)])),
      (0, _operators.catchError)((error) => (0, _rxjs.of)(onBinanceBalancesError(error)))
    ))
  )
}; exports.binanceBalancesRequestEpic = binanceBalancesRequestEpic
