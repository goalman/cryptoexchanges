const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.balancesAndAddressesRequestEpic = exports.balancesAndAddressesReducer = exports.onBalancesAndAddressesSuccess = exports.onBalancesAndAddressesRequest = exports.initialState = void 0; const _defineProperty2 = _interopRequireDefault(require('@babel/runtime/helpers/defineProperty'))
const _rxjs = require('rxjs')
const _operators = require('rxjs/operators')


const _reduxObservable = require('redux-observable')
const _balance = _interopRequireDefault(require('../services/binance/balance'))
const _balance2 = _interopRequireDefault(require('../services/bittrex/balance'))
const _balance3 = _interopRequireDefault(require('../services/bitfinex/balance'))
const _balance4 = _interopRequireDefault(require('../services/huobi/balance'))
const _prices = _interopRequireDefault(require('../services/cryptoCompare/prices'))
const _binance = require('../mixins/binance')
const _bitfinex = require('../mixins/bitfinex')
const _huobi = require('../mixins/huobi')

const _bittrex = require('../mixins/bittrex')


const _cryptocompare = require('../mixins/cryptocompare')
const _mixin = require('../mixins/mixin')

function ownKeys(object, enumerableOnly) { const keys = Object.keys(object); if (Object.getOwnPropertySymbols) { let symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly)symbols = symbols.filter((sym) => Object.getOwnPropertyDescriptor(object, sym).enumerable); keys.push.apply(keys, symbols) } return keys } function _objectSpread(target) { for (let i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach((key) => { (0, _defineProperty2.default)(target, key, source[key]) }) } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) } else { ownKeys(source).forEach((key) => { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)) }) } } return target }

const initialState = {
  balancesAndAddresses: {},
  loading: false
}; exports.initialState = initialState


const onBalancesAndAddressesRequest = function onBalancesAndAddressesRequest(currencySymbol, currencyName) {
  return {
    type: 'ON_BALANCES_AND_ADDRESSES_REQUEST',
    currencySymbol,
    currencyName
  }
}; exports.onBalancesAndAddressesRequest = onBalancesAndAddressesRequest

const onBalancesAndAddressesSuccess = function onBalancesAndAddressesSuccess(balancesAndAddresses) {
  return {
    type: 'ON_BALANCES_AND_ADDRESSES_SUCCESS',
    balancesAndAddresses
  }
}; exports.onBalancesAndAddressesSuccess = onBalancesAndAddressesSuccess


const balancesAndAddressesReducer = function balancesAndAddressesReducer() {
  const state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState; const action = arguments.length > 1 ? arguments[1] : undefined
  switch (action.type) {
  case 'ON_BALANCES_AND_ADDRESSES_REQUEST':
    return _objectSpread({},
      state, { loading: true })

  case 'ON_BALANCES_AND_ADDRESSES_SUCCESS':
    return _objectSpread({},
      state, {
        balancesAndAddresses: action.balancesAndAddresses,
        loading: false
      })

  default:
    return state
  }
}; exports.balancesAndAddressesReducer = balancesAndAddressesReducer

const balancesAndAddressesRequestEpic = function balancesAndAddressesRequestEpic(action$) {
  return action$.pipe(
    (0, _reduxObservable.ofType)('ON_BALANCES_AND_ADDRESSES_REQUEST'),
    (0, _operators.switchMap)((action) => (0, _rxjs.forkJoin)({
      binanceBalance: (0, _rxjs.from)(_balance.default.getBalances((0, _binance.getBinanceBalancesParams)())).pipe(
        (0, _operators.map)((response) => (0, _binance.filterBinanceBalances)(response.data.balances, [action.currencySymbol])),
        (0, _operators.map)((balances) => (0, _binance.mapBinanceBalances)(balances)),
        (0, _operators.map)((balances) => balances[0]),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      bittrexBalance: (0, _rxjs.from)(_balance2.default.getBalance((0, _bittrex.getBittrexBalanceParams)(action.currencySymbol))).pipe(
        (0, _operators.map)((response) => response.data.result),
        (0, _operators.map)((balances) => (0, _bittrex.mapBittrexBalances)([balances])),
        (0, _operators.map)((balances) => balances[0]),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      bitfinexBalance: (0, _rxjs.from)(_balance3.default.getBalances()).pipe(
        (0, _operators.map)((response) => (0, _bitfinex.filterBitfinexBalances)(response.data, [action.currencySymbol])),
        (0, _operators.map)((balances) => (0, _bitfinex.mapBitfinexBalances)(balances, [action.currencySymbol])),
        (0, _operators.map)((balances) => balances[0]),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      huobiBalance: (0, _rxjs.from)(_balance4.default.getHuobiAccountBalances()).pipe(
        (0, _operators.map)((response) => (0, _huobi.filterHuobiBalances)(response.data.data.list, [action.currencySymbol])),
        (0, _operators.map)((balances) => (0, _huobi.mapHuobiBalances)(balances, [action.currencySymbol])),
        (0, _operators.map)((balances) => balances[0]),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      czkRates: (0, _rxjs.from)(_prices.default.getPrices((0, _cryptocompare.getCryptocomaparePricesParams)(
        [{
          name: action.currencyName,
          symbol: action.currencySymbol
        }]
      )))

        .pipe(
          (0, _operators.map)((response) => response.data),
          (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
        ),

      binanceAddress: (0, _rxjs.from)(_balance.default.getDepositAddress((0, _binance.getBinanceAddressParams)(action.currencySymbol))).pipe(
        (0, _operators.map)((response) => response.data.address),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      bittrexAddress: (0, _rxjs.from)(_balance2.default.getDepositAddress((0, _bittrex.getBittrexAddressParams)(action.currencySymbol))).pipe(
        (0, _operators.map)((response) => response.data.result.Address),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      bitfinexAddress: (0, _rxjs.from)(_balance3.default.getDepositAddress((0, _bitfinex.getBitfinexAddressData)(action.currencyName))).pipe(
        (0, _operators.map)((response) => response.data.address),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      huobiAddress: (0, _rxjs.from)(_balance4.default.getDepositAddress((0, _huobi.getHuobiAddressParams)(action.currencySymbol))).pipe(
        (0, _operators.map)((response) => response.data.data[0].address),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      )
    })),


    (0, _operators.map)((balancesAndAddresses) => (0, _mixin.countCzkDetailPrices)(balancesAndAddresses)),
    (0, _operators.flatMap)((balancesAndAddresses) => (0, _rxjs.from)([onBalancesAndAddressesSuccess(balancesAndAddresses)]))
  )
}; exports.balancesAndAddressesRequestEpic = balancesAndAddressesRequestEpic
