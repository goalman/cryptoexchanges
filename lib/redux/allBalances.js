const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.allBalancesRequestEpic = exports.allBalancesReducer = exports.onAllBalancesSuccess = exports.onAllBalancesRequest = exports.initialState = void 0; const _defineProperty2 = _interopRequireDefault(require('@babel/runtime/helpers/defineProperty')); const _rxjs = require('rxjs')
const _operators = require('rxjs/operators')


const _reduxObservable = require('redux-observable')
const _config = _interopRequireDefault(require('../services/config'))
const _balance = _interopRequireDefault(require('../services/binance/balance'))
const _balance2 = _interopRequireDefault(require('../services/bittrex/balance'))
const _balance3 = _interopRequireDefault(require('../services/bitfinex/balance'))
const _balance4 = _interopRequireDefault(require('../services/huobi/balance'))
const _prices = _interopRequireDefault(require('../services/cryptoCompare/prices'))
const _binance = require('../mixins/binance')
const _bitfinex = require('../mixins/bitfinex')
const _huobi = require('../mixins/huobi')
const _bittrex = require('../mixins/bittrex')
const _mixin = require('../mixins/mixin')

function ownKeys(object, enumerableOnly) { const keys = Object.keys(object); if (Object.getOwnPropertySymbols) { let symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly)symbols = symbols.filter((sym) => Object.getOwnPropertyDescriptor(object, sym).enumerable); keys.push.apply(keys, symbols) } return keys } function _objectSpread(target) { for (let i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach((key) => { (0, _defineProperty2.default)(target, key, source[key]) }) } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) } else { ownKeys(source).forEach((key) => { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)) }) } } return target }

const initialState = { allBalances: {} }; exports.initialState = initialState


const onAllBalancesRequest = function onAllBalancesRequest() { return { type: 'ON_ALL_BALANCES_REQUEST' } }; exports.onAllBalancesRequest = onAllBalancesRequest

const onAllBalancesSuccess = function onAllBalancesSuccess(allBalances) {
  return {
    type: 'ON_ALL_BALANCES_SUCCESS',
    allBalances
  }
}; exports.onAllBalancesSuccess = onAllBalancesSuccess


const allBalancesReducer = function allBalancesReducer() {
  const state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState; const action = arguments.length > 1 ? arguments[1] : undefined
  switch (action.type) {
  case 'ON_ALL_BALANCES_REQUEST':
    return _objectSpread({},
      state)

  case 'ON_ALL_BALANCES_SUCCESS':
    return _objectSpread({},
      state, { allBalances: action.allBalances })

  default:
    return state
  }
}; exports.allBalancesReducer = allBalancesReducer

const allBalancesRequestEpic = function allBalancesRequestEpic(action$) {
  return action$.pipe(
    (0, _reduxObservable.ofType)('ON_ALL_BALANCES_REQUEST'),
    (0, _operators.switchMap)(() => (0, _rxjs.forkJoin)({
      binanceBalances: (0, _rxjs.from)(_balance.default.getBalances((0, _binance.getBinanceBalancesParams)())).pipe(
        (0, _operators.map)((response) => (0, _binance.filterBinanceBalances)(response.data.balances, _config.default.binance.currencies)),
        (0, _operators.map)((balances) => (0, _binance.mapBinanceBalances)(balances)),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      bittrexBalances: (0, _rxjs.from)(_balance2.default.getSpecificBalances(_config.default.bittrex.currencies)).pipe(
        (0, _operators.map)((balances) => (0, _bittrex.mapBittrexBalances)(balances)),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      bitfinexBalances: (0, _rxjs.from)(_balance3.default.getBalances()).pipe(
        (0, _operators.map)((response) => (0, _bitfinex.filterBitfinexBalances)(response.data, _config.default.bitfinex.currencies)),
        (0, _operators.map)((balances) => (0, _bitfinex.mapBitfinexBalances)(balances, _config.default.bitfinex.currencies)),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      huobiBalances: (0, _rxjs.from)(_balance4.default.getHuobiAccountBalances()).pipe(
        (0, _operators.map)((response) => (0, _huobi.filterHuobiBalances)(response.data.data.list, _config.default.huobi.currencies)),
        (0, _operators.map)((balances) => (0, _huobi.mapHuobiBalances)(balances, _config.default.huobi.currencies)),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      ),

      czkRates: (0, _rxjs.from)(_prices.default.getSupportedCurrenciesCzkRates()).pipe(
        (0, _operators.map)((response) => response.data),
        (0, _operators.catchError)((error) => (0, _rxjs.of)(error))
      )
    })),


    (0, _operators.map)((allBalances) => (0, _mixin.sumBalances)(allBalances)),
    (0, _operators.map)((allBalances) => (0, _mixin.countCzkPrices)(allBalances)),
    (0, _operators.map)((allBalances) => (0, _mixin.countTotalCzkBalance)(allBalances)),
    (0, _operators.map)((allBalances) => (0, _mixin.sortAllBalances)(allBalances)),
    (0, _operators.flatMap)((allBalances) => (0, _rxjs.from)([onAllBalancesSuccess(allBalances)]))
  )
}; exports.allBalancesRequestEpic = allBalancesRequestEpic
