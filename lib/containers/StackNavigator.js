var _interopRequireDefault=require("@babel/runtime/helpers/interopRequireDefault");Object.defineProperty(exports,"__esModule",{value:true});exports.default=void 0;var _reactNavigation=require("react-navigation");
var _BalanceDetailScreen=_interopRequireDefault(require("./screens/BalanceDetailScreen"));
var _BalancesScreen=_interopRequireDefault(require("./screens/BalancesScreen"));

var BalanceStack=(0,_reactNavigation.createStackNavigator)({
Balances:{
screen:_BalancesScreen.default,
navigationOptions:{
header:null}},


Actions:{
screen:_BalanceDetailScreen.default,
navigationOptions:function navigationOptions(_ref){var navigation=_ref.navigation;return{
title:navigation.state.params.currencyName};}}});var _default=




BalanceStack;exports.default=_default;