var _interopRequireDefault=require("@babel/runtime/helpers/interopRequireDefault");Object.defineProperty(exports,"__esModule",{value:true});exports.default=void 0;var _defineProperty2=_interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));var _toConsumableArray2=_interopRequireDefault(require("@babel/runtime/helpers/toConsumableArray"));var _classCallCheck2=_interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));var _createClass2=_interopRequireDefault(require("@babel/runtime/helpers/createClass"));var _possibleConstructorReturn2=_interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));var _getPrototypeOf2=_interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));var _inherits2=_interopRequireDefault(require("@babel/runtime/helpers/inherits"));
var _reactNative=require("react-native");



var _react=_interopRequireDefault(require("react"));
var _redux=require("redux");
var _reactRedux=require("react-redux");
var _reactNavigation=require("react-navigation");
var _reactNativePaper=require("react-native-paper");
var _TopStatusBar=_interopRequireDefault(require("../../components/TopStatusBar"));
var _balancesAndAddresses=require("../../redux/balancesAndAddresses");
var _index=_interopRequireDefault(require("../../themes/index"));
var _balanceDetailScreen=_interopRequireDefault(require("../../themes/balanceDetailScreen"));
var _infoBar=_interopRequireDefault(require("../../themes/infoBar"));
var _InfoBar=_interopRequireDefault(require("../../components/InfoBar"));
var _filters=require("../../mixins/filters");


var _rules=require("../../mixins/rules");


var _binance=require("../../mixins/binance");
var _config=_interopRequireDefault(require("../../services/config"));
var _balance=_interopRequireDefault(require("../../services/binance/balance"));var _jsxFileName="C:\\Users\\tiboy\\IdeaProjects\\cryptoexchanges\\src\\containers\\screens\\BalanceDetailScreen.js";var






BalanceDetailScreen=function(_React$Component){(0,_inherits2.default)(BalanceDetailScreen,_React$Component);
function BalanceDetailScreen(props){var _this;(0,_classCallCheck2.default)(this,BalanceDetailScreen);
_this=(0,_possibleConstructorReturn2.default)(this,(0,_getPrototypeOf2.default)(BalanceDetailScreen).call(this,props));_this.











































































































































_onRefresh=function(){
_this.setState(function(){return{refreshing:true};});
_this.loadBalancesAndAddresses();
};_this.state={refreshing:false,exchangeSelectedFrom:'',exchangeSelectedTo:'',amountToSend:'',maxAmountToSend:0,withdrawAddress:'',amountToSendErrorText:'',withdrawAddressErrorText:'',errors:[],successes:[]};return _this;}(0,_createClass2.default)(BalanceDetailScreen,[{key:"componentDidMount",value:function componentDidMount(){this.loadBalancesAndAddresses();this.setState(function(){return{refreshing:true};});}},{key:"componentWillReceiveProps",value:function componentWillReceiveProps(nextProps,nextContext){this.setState(function(){return{errors:nextProps.balancesAndAddresses.errors};});if(this.state.exchangeSelectedFrom===''){this.setState(function(){return{exchangeSelectedFrom:'Binance'};});var maxAmountToSend=(0,_filters.decimalPointToComma)(nextProps.balancesAndAddresses.binanceBalance.available);this.setState(function(){return{maxAmountToSend:maxAmountToSend};});}if(this.state.exchangeSelectedTo===''){this.setState(function(){return{exchangeSelectedTo:'Binance'};});this.setState(function(){return{withdrawAddress:nextProps.balancesAndAddresses.binanceAddress};});}this.setState(function(){return{refreshing:false};});}},{key:"loadBalancesAndAddresses",value:function loadBalancesAndAddresses(){var _this$props$navigatio=this.props.navigation.state.params,currencySymbol=_this$props$navigatio.currencySymbol,currencyName=_this$props$navigatio.currencyName;this.props.onBalancesAndAddressesRequest(currencySymbol,currencyName);}},{key:"withdrawBinanceBalance",value:function withdrawBinanceBalance(){var _this2=this;var _this$props$navigatio2=this.props.navigation.state.params,currencySymbol=_this$props$navigatio2.currencySymbol,currencyName=_this$props$navigatio2.currencyName;this.setState(function(){return{refreshing:true};});_balance.default.withdrawBalance((0,_binance.getBinanceWithdrawParams)(currencySymbol,this.state.amountToSend,this.state.withdrawAddress)).then(function(response){_this2.setState(function(){return{amountToSend:''};});var binanceSuccessText={Binance:"Z\u016Fstatek byl odesl\xE1n na "+_this2.state.exchangeSelectedTo+"."};_this2.setState(function(){return{successes:[].concat((0,_toConsumableArray2.default)(_this2.state.successes),[binanceSuccessText])};});_this2.props.onBalancesAndAddressesRequest(currencySymbol,currencyName);}).catch(function(error){var binanceErrorText={Binance:'Zůstatek se nepodařilo odeslat.'};_this2.setState(function(){return{errors:[].concat((0,_toConsumableArray2.default)(_this2.state.errors),[binanceErrorText])};});}).finally(function(){_this2.setState(function(){return{refreshing:false};});});}},{key:"withdrawBalance",value:function withdrawBalance(){if(this.state.exchangeSelectedFrom==='Binance'){this.withdrawBinanceBalance();}}},{key:"checkFormValues",value:function checkFormValues(){var _this$props$navigatio3=this.props.navigation.state.params,currencySymbol=_this$props$navigatio3.currencySymbol,currencyName=_this$props$navigatio3.currencyName;console.log(currencySymbol.toLowerCase());var errors=[];if(!(0,_rules.validDecimalNumber)((0,_filters.decimalCommaToPoint)(this.state.amountToSend))){errors.push(false);this.setState(function(){return{amountToSendErrorText:'Zadejte číslo s desetinou čárkou nebo tečkou.'};});}else if(!(0,_rules.numberIsSmallerOrEqual)((0,_filters.decimalCommaToPoint)(this.state.amountToSend),(0,_filters.decimalCommaToPoint)(this.state.maxAmountToSend))){errors.push(false);this.setState(function(){return{amountToSendErrorText:'Hodnota nesmí být větší než dostupný zůstatek.'};});}else if(!(0,_rules.numberIsBigger)((0,_filters.decimalCommaToPoint)(this.state.amountToSend),0)){errors.push(false);this.setState(function(){return{amountToSendErrorText:'Hodnota musí být větší než nula.'};});}else if(this.state.exchangeSelectedFrom===this.state.exchangeSelectedTo){errors.push(false);this.setState(function(){return{amountToSendErrorText:'Vybrané burzy musí být rozdílné'};});}else{this.setState(function(){return{amountToSendErrorText:''};});}if(!(0,_rules.validCurrencyAddress)(this.state.withdrawAddress,currencySymbol)){errors.push(false);this.setState(function(){return{withdrawAddressErrorText:"Adresa mus\xED m\xEDt validn\xED form\xE1t kryptom\u011Bny "+currencyName+"."};});}else{this.setState(function(){return{withdrawAddressErrorText:''};});}return!errors.length;}},{key:"_setExchangeSelectedFrom",value:function _setExchangeSelectedFrom(selectedExchange){var currencySymbol=this.props.navigation.state.params.currencySymbol;this.setState(function(){return{exchangeSelectedFrom:selectedExchange};});var maxAmountToSend=(0,_filters.decimalPointToComma)(this.props.balancesAndAddresses[selectedExchange.toLowerCase()+"Balance"].available);this.setState(function(){return{maxAmountToSend:maxAmountToSend};});}},{key:"_setExchangeSelectedTo",value:function _setExchangeSelectedTo(selectedExchange){this.setState(function(){return{exchangeSelectedTo:selectedExchange};});var withdrawAddress=selectedExchange!=='Vlastní adresa'?this.props.balancesAndAddresses[selectedExchange.toLowerCase()+"Address"]:'';this.setState(function(){return{withdrawAddress:withdrawAddress};});}},{key:"_onButtonSendClick",value:function _onButtonSendClick(){var _this3=this;var currencySymbol=this.props.navigation.state.params.currencySymbol;if(this.checkFormValues()){_reactNative.Alert.alert('Prosím potvrďte převod',this.state.amountToSend+" "+currencySymbol+"  na "+this.state.exchangeSelectedTo+"\n        \nAdresa: \n"+this.state.withdrawAddress,[{text:'Zavřít',style:'cancel'},{text:'Odeslat',onPress:function onPress(){return _this3.withdrawBalance();}}],{cancelable:true});}}},{key:"render",value:function render()

{var _this4=this;var _this$props$balancesA=


this.props.balancesAndAddresses,binanceBalance=_this$props$balancesA.binanceBalance,errors=_this$props$balancesA.errors;
return(
_react.default.createElement(_reactNative.SafeAreaView,{style:_balanceDetailScreen.default.container,__source:{fileName:_jsxFileName,lineNumber:184}},
_react.default.createElement(_reactNative.ScrollView,{
contentContainerStyle:{flex:1},
refreshControl:
_react.default.createElement(_reactNative.RefreshControl,{
refreshing:this.state.refreshing,
onRefresh:this._onRefresh,__source:{fileName:_jsxFileName,lineNumber:188}}),__source:{fileName:_jsxFileName,lineNumber:185}},



_react.default.createElement(_TopStatusBar.default,{backgroundColor:"#772ea2",barStyle:"light-content",__source:{fileName:_jsxFileName,lineNumber:194}}),
_react.default.createElement(_reactNative.View,{__source:{fileName:_jsxFileName,lineNumber:195}},
_react.default.createElement(Error,{errors:this.state.errors,__source:{fileName:_jsxFileName,lineNumber:196}}),
_react.default.createElement(Success,{successes:this.state.successes,__source:{fileName:_jsxFileName,lineNumber:197}}),
_react.default.createElement(_reactNativePaper.DataTable,{style:_balanceDetailScreen.default.dataTable,__source:{fileName:_jsxFileName,lineNumber:198}},
_react.default.createElement(_reactNativePaper.DataTable.Header,{__source:{fileName:_jsxFileName,lineNumber:199}},
_react.default.createElement(_reactNativePaper.DataTable.Title,{__source:{fileName:_jsxFileName,lineNumber:200}},_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.dataTableTitleNameCell,__source:{fileName:_jsxFileName,lineNumber:200}},"Burza")),
_react.default.createElement(_reactNativePaper.DataTable.Title,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:201}},_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.dataTableTitleCell,__source:{fileName:_jsxFileName,lineNumber:201}},"Dostupn\xFD")),
_react.default.createElement(_reactNativePaper.DataTable.Title,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:202}},_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.dataTableTitleCell,__source:{fileName:_jsxFileName,lineNumber:202}},"Celkem")),
_react.default.createElement(_reactNativePaper.DataTable.Title,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:203}},_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.dataTableTitleCell,__source:{fileName:_jsxFileName,lineNumber:203}},"Celkem v K\u010D "))),

_react.default.createElement(TableRows,{
binanceBalance:binanceBalance,__source:{fileName:_jsxFileName,lineNumber:205}}),

_react.default.createElement(_reactNativePaper.DataTable.Pagination,{style:_index.default.invisible,__source:{fileName:_jsxFileName,lineNumber:208}}))),


_react.default.createElement(_reactNative.View,{style:_balanceDetailScreen.default.withdrawContainer,__source:{fileName:_jsxFileName,lineNumber:211}},
_react.default.createElement(_reactNative.View,{style:_balanceDetailScreen.default.headBox,__source:{fileName:_jsxFileName,lineNumber:212}},
_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.headBoxLabelText,__source:{fileName:_jsxFileName,lineNumber:213}},"Z burzy:"),
_react.default.createElement(_reactNative.Picker,{
selectedValue:this.state.exchangeSelectedFrom,
style:_balanceDetailScreen.default.picker,
onValueChange:function onValueChange(selectedValue){return _this4._setExchangeSelectedFrom(selectedValue);},__source:{fileName:_jsxFileName,lineNumber:214}},

PickerValuesExchangeFrom())),


_react.default.createElement(_reactNative.View,{style:_balanceDetailScreen.default.inputBoxAmount,__source:{fileName:_jsxFileName,lineNumber:222}},
_react.default.createElement(_reactNative.Text,{__source:{fileName:_jsxFileName,lineNumber:223}},"Mno\u017Estv\xED:"),
_react.default.createElement(_reactNative.TextInput,{
style:_balanceDetailScreen.default.inputAmountValue,
placeholder:"0,00",
onChangeText:function onChangeText(value){return _this4.setState(function(){return{amountToSend:value};});},
value:this.state.amountToSend.toString(),
keyboardType:"numeric",__source:{fileName:_jsxFileName,lineNumber:224}}),

_react.default.createElement(_reactNative.Text,{__source:{fileName:_jsxFileName,lineNumber:231}},"/",

this.state.maxAmountToSend),

_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.inputErrorText,__source:{fileName:_jsxFileName,lineNumber:235}},this.state.amountToSendErrorText)),

_react.default.createElement(_reactNative.View,{style:_balanceDetailScreen.default.headBox,__source:{fileName:_jsxFileName,lineNumber:237}},
_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.headBoxLabelText,__source:{fileName:_jsxFileName,lineNumber:238}},"Na burzu (adresu):"),
_react.default.createElement(_reactNative.Picker,{
selectedValue:this.state.exchangeSelectedTo,
style:_balanceDetailScreen.default.picker,
onValueChange:function onValueChange(selectedValue){return _this4._setExchangeSelectedTo(selectedValue);},__source:{fileName:_jsxFileName,lineNumber:239}},

PickerValuesExchangeTo())),


_react.default.createElement(_reactNative.View,{style:_balanceDetailScreen.default.inputBoxAddress,__source:{fileName:_jsxFileName,lineNumber:247}},
_react.default.createElement(_reactNative.TextInput,{
placeholder:"1BvBMSEYstWetqTFn5Au4m4GFg7xJaNVN2",
onChangeText:function onChangeText(address){return _this4.setState(function(){return{withdrawAddress:address};});},
editable:this.state.exchangeSelectedTo==='Vlastní adresa',
value:this.state.withdrawAddress,__source:{fileName:_jsxFileName,lineNumber:248}}),

_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.inputErrorText,__source:{fileName:_jsxFileName,lineNumber:254}},this.state.withdrawAddressErrorText)),

_react.default.createElement(_reactNative.TouchableOpacity,{
style:_balanceDetailScreen.default.sendButton,
onPress:function onPress(){
_this4._onButtonSendClick();
},__source:{fileName:_jsxFileName,lineNumber:256}},

_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.sendButtonText,__source:{fileName:_jsxFileName,lineNumber:262}},"Odeslat"))))));





}}]);return BalanceDetailScreen;}(_react.default.Component);


var Success=function Success(props){var
successes=props.successes;
if(successes&&successes.length){
return(
_react.default.createElement(_reactNative.View,{style:_infoBar.default.container,__source:{fileName:_jsxFileName,lineNumber:275}},
successes.map(function(success){return(
_react.default.createElement(_InfoBar.default,{
subject:Object.keys(success)[0],
message:Object.values(success)[0],
reason:"success",
key:Math.random().toString(),__source:{fileName:_jsxFileName,lineNumber:277}}));})));




}
return null;
};

var getErrorsTexts=function getErrorsTexts(errors){
var errorsTexts=[];
errors.forEach(function(error){
var foundExchange=_config.default.exchanges.find(function(exchange){return Object.keys(error)[0].includes(exchange.toLowerCase());});
var subject=foundExchange||'Cryptocompare';
var message=Object.keys(error)[0].includes('Balance')?
'Zůstatek kryptoměny se nepodařilo načíst.':
Object.keys(error)[0].includes('Address')?
'Adresu pro převod kryptoměny se nepodařilo načíst.':
'Aktuální kurz kryptoměny se nepodařilo načíst.';
message+=' Potažením dolů opakujte pokus.';
errorsTexts.push((0,_defineProperty2.default)({},subject,message));
});
return errorsTexts;
};

var Error=function Error(props){var
errors=props.errors;
if(errors&&errors.length){
return(
_react.default.createElement(_reactNative.View,{style:_infoBar.default.container,__source:{fileName:_jsxFileName,lineNumber:310}},
getErrorsTexts(errors).map(function(error){return(
_react.default.createElement(_InfoBar.default,{
subject:Object.keys(error)[0],
message:Object.values(error)[0],
reason:"error",
key:Math.random().toString(),__source:{fileName:_jsxFileName,lineNumber:312}}));})));




}
return null;
};

var TableRows=function TableRows(props){var

binanceBalance=
props.binanceBalance;
if(binanceBalance){
var exchanges=[
{name:'Binance',balance:binanceBalance}];

return exchanges.map(function(exchange){return(
_react.default.createElement(_reactNativePaper.DataTable.Row,{style:_balanceDetailScreen.default.dataTableRow,key:exchange.name,__source:{fileName:_jsxFileName,lineNumber:334}},
_react.default.createElement(_reactNativePaper.DataTable.Cell,{__source:{fileName:_jsxFileName,lineNumber:335}},_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.dataTableNameCell,__source:{fileName:_jsxFileName,lineNumber:335}},exchange.name)),
_react.default.createElement(_reactNativePaper.DataTable.Cell,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:336}},_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.dataTableCell,__source:{fileName:_jsxFileName,lineNumber:336}},(0,_filters.numberFormatToShow)(exchange.balance.available))),
_react.default.createElement(_reactNativePaper.DataTable.Cell,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:337}},_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.dataTableCell,__source:{fileName:_jsxFileName,lineNumber:337}},(0,_filters.numberFormatToShow)(exchange.balance.balance))),
_react.default.createElement(_reactNativePaper.DataTable.Cell,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:338}},_react.default.createElement(_reactNative.Text,{style:_balanceDetailScreen.default.dataTableCell,__source:{fileName:_jsxFileName,lineNumber:338}},(0,_filters.numberFormatToShow)((0,_filters.roundNumber)(exchange.balance.czkBalance,2))))));});


}
return(
_react.default.createElement(ProgressBar,{
binanceBalance:binanceBalance,__source:{fileName:_jsxFileName,lineNumber:343}}));


};

var ProgressBar=function ProgressBar(props){var

binanceBalance=
props.binanceBalance;
if(!binanceBalance){
if(_reactNative.Platform.OS==='ios')return _react.default.createElement(_reactNative.ProgressViewIOS,{__source:{fileName:_jsxFileName,lineNumber:354}});
return _react.default.createElement(_reactNative.ProgressBarAndroid,{__source:{fileName:_jsxFileName,lineNumber:355}});
}
return null;
};

var PickerValuesExchangeFrom=function PickerValuesExchangeFrom(){return(
_config.default.exchanges.map(function(exchange){return _react.default.createElement(_reactNative.Picker.Item,{label:exchange,value:exchange,key:exchange,__source:{fileName:_jsxFileName,lineNumber:361}});}));};


var PickerValuesExchangeTo=function PickerValuesExchangeTo(){
var values=[].concat((0,_toConsumableArray2.default)(_config.default.exchanges),['Vlastní adresa']);
return values.map(function(exchange){return _react.default.createElement(_reactNative.Picker.Item,{label:exchange,value:exchange,key:exchange,__source:{fileName:_jsxFileName,lineNumber:366}});});
};

var mapStateToProps=function mapStateToProps(state){return{
balancesAndAddresses:state.balancesAndAddressesReducer.balancesAndAddresses};};


var mapDispatchToProps=function mapDispatchToProps(dispatch){return(0,_redux.bindActionCreators)({onBalancesAndAddressesRequest:_balancesAndAddresses.onBalancesAndAddressesRequest},dispatch);};var _default=

(0,_reactRedux.connect)(mapStateToProps,mapDispatchToProps)(BalanceDetailScreen);exports.default=_default;