const _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault')

Object.defineProperty(exports, '__esModule', { value: true }); exports.default = void 0; const _classCallCheck2 = _interopRequireDefault(require('@babel/runtime/helpers/classCallCheck')); const _createClass2 = _interopRequireDefault(require('@babel/runtime/helpers/createClass')); const _possibleConstructorReturn2 = _interopRequireDefault(require('@babel/runtime/helpers/possibleConstructorReturn')); const _getPrototypeOf2 = _interopRequireDefault(require('@babel/runtime/helpers/getPrototypeOf')); const _inherits2 = _interopRequireDefault(require('@babel/runtime/helpers/inherits'))
const _reactNative = require('react-native')


const _react = _interopRequireDefault(require('react'))
const _redux = require('redux')
const _reactRedux = require('react-redux')
const _allBalances = require('../../redux/allBalances')

const _jsxFileName = 'C:\\users\\tiboy\\IdeaProjects\\cryptotradition\\src\\containers\\screens\\BalanceActionsScreen.js'; const


  BalanceActionsScreen = (function (_React$Component) {
    (0, _inherits2.default)(BalanceActionsScreen, _React$Component); function BalanceActionsScreen() { (0, _classCallCheck2.default)(this, BalanceActionsScreen); return (0, _possibleConstructorReturn2.default)(this, (0, _getPrototypeOf2.default)(BalanceActionsScreen).apply(this, arguments)) }(0, _createClass2.default)(BalanceActionsScreen, [{
      key: 'componentDidMount',
      value: function componentDidMount() {
        this.loadBalances()
      }
    }, {
      key: 'loadBalances',
      value: function loadBalances() {
        this.props.onBinanceBalancesRequest()
      }
    }, {
      key: 'render',
      value: function render() {
        return (
          _react.default.createElement(_reactNative.Text, { __source: { fileName: _jsxFileName, lineNumber: 24 } }, this.props.text))
      }
    }]); return BalanceActionsScreen
  }(_react.default.Component))


const mapStateToProps = function mapStateToProps(state) { return { balancesAndAddresses: state.balancesAndAddressesReducer.balancesAndAddresses } }


const mapDispatchToProps = function mapDispatchToProps(dispatch) { return (0, _redux.bindActionCreators)({ onAllBalancesRequest: _allBalances.onAllBalancesRequest }, dispatch) }; const _default = (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)(BalanceActionsScreen); exports.default = _default
