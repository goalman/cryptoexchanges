var _interopRequireDefault=require("@babel/runtime/helpers/interopRequireDefault");Object.defineProperty(exports,"__esModule",{value:true});exports.default=void 0;var _defineProperty2=_interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));var _regenerator=_interopRequireDefault(require("@babel/runtime/regenerator"));var _classCallCheck2=_interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));var _createClass2=_interopRequireDefault(require("@babel/runtime/helpers/createClass"));var _possibleConstructorReturn2=_interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));var _getPrototypeOf2=_interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));var _inherits2=_interopRequireDefault(require("@babel/runtime/helpers/inherits"));
var _reactNative=require("react-native");


var _react=_interopRequireDefault(require("react"));
var _reactNativePaper=require("react-native-paper");
var _reactRedux=require("react-redux");
var _redux=require("redux");
var _index=_interopRequireDefault(require("../../themes/index"));
var _balancesScreen=_interopRequireDefault(require("../../themes/balancesScreen"));
var _TopStatusBar=_interopRequireDefault(require("../../components/TopStatusBar"));
var _InfoBar=_interopRequireDefault(require("../../components/InfoBar"));
var _allBalances=require("../../redux/allBalances");
var _filters=require("../../mixins/filters");


var _infoBar=_interopRequireDefault(require("../../themes/infoBar"));
var _config=_interopRequireDefault(require("../../services/config"));var _jsxFileName="C:\\Users\\tiboy\\IdeaProjects\\cryptoexchanges\\src\\containers\\screens\\BalancesScreen.js";var






BalancesScreen=function(_React$Component){(0,_inherits2.default)(BalancesScreen,_React$Component);
function BalancesScreen(props){var _this;(0,_classCallCheck2.default)(this,BalancesScreen);
_this=(0,_possibleConstructorReturn2.default)(this,(0,_getPrototypeOf2.default)(BalancesScreen).call(this,props));_this.














loadBalances=function(){
_this.props.onAllBalancesRequest();
};_this.

_onRefresh=function(){
_this.setState(function(){return{refreshing:true};});
_this.loadBalances();
};_this.state={refreshing:false};return _this;}(0,_createClass2.default)(BalancesScreen,[{key:"componentDidMount",value:function componentDidMount(){return _regenerator.default.async(function componentDidMount$(_context){while(1){switch(_context.prev=_context.next){case 0:this.setState(function(){return{refreshing:true};});this.loadBalances();case 2:case"end":return _context.stop();}}},null,this);}},{key:"componentWillReceiveProps",value:function componentWillReceiveProps(nextProps,nextContext){this.setState(function(){return{refreshing:false};});}},{key:"render",value:function render()

{var _this$props$allBalanc=
this.props.allBalances,totalCzkBalance=_this$props$allBalanc.totalCzkBalance,balances=_this$props$allBalanc.balances,errors=_this$props$allBalanc.errors;var
navigate=this.props.navigation.navigate;
return(
_react.default.createElement(_reactNative.SafeAreaView,{style:_balancesScreen.default.container,__source:{fileName:_jsxFileName,lineNumber:55}},
_react.default.createElement(_reactNative.ScrollView,{
contentContainerStyle:{flex:1},
refreshControl:
_react.default.createElement(_reactNative.RefreshControl,{
refreshing:this.state.refreshing,
onRefresh:this._onRefresh,__source:{fileName:_jsxFileName,lineNumber:59}}),__source:{fileName:_jsxFileName,lineNumber:56}},



_react.default.createElement(_TopStatusBar.default,{backgroundColor:"#772ea2",barStyle:"light-content",__source:{fileName:_jsxFileName,lineNumber:65}}),
_react.default.createElement(_reactNative.View,{__source:{fileName:_jsxFileName,lineNumber:66}},
_react.default.createElement(Error,{errors:errors,__source:{fileName:_jsxFileName,lineNumber:67}}),
_react.default.createElement(_reactNative.Text,{style:_balancesScreen.default.header,__source:{fileName:_jsxFileName,lineNumber:68}},"Prost\u0159edky"),
_react.default.createElement(TotalBalance,{balances:balances,totalCzkBalance:totalCzkBalance,__source:{fileName:_jsxFileName,lineNumber:69}}),
_react.default.createElement(_reactNativePaper.DataTable,{style:_balancesScreen.default.dataTable,__source:{fileName:_jsxFileName,lineNumber:70}},
_react.default.createElement(_reactNativePaper.DataTable.Header,{__source:{fileName:_jsxFileName,lineNumber:71}},
_react.default.createElement(_reactNativePaper.DataTable.Title,{__source:{fileName:_jsxFileName,lineNumber:72}},
_react.default.createElement(_reactNative.Text,{
onPress:function onPress(){
alert('You tapped the button!');
},
style:_balancesScreen.default.dataTableTitleNameCell,__source:{fileName:_jsxFileName,lineNumber:73}},"N\xE1zev")),




_react.default.createElement(_reactNativePaper.DataTable.Title,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:82}},_react.default.createElement(_reactNative.Text,{style:_balancesScreen.default.dataTableTitleCell,__source:{fileName:_jsxFileName,lineNumber:82}},"Po\u010Det ks")),
_react.default.createElement(_reactNativePaper.DataTable.Title,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:83}},_react.default.createElement(_reactNative.Text,{style:_balancesScreen.default.dataTableTitleCell,__source:{fileName:_jsxFileName,lineNumber:83}},"Hodnota K\u010D"))),

_react.default.createElement(TableRows,{balances:balances,navigate:navigate,__source:{fileName:_jsxFileName,lineNumber:85}}),
_react.default.createElement(_reactNativePaper.DataTable.Pagination,{style:_index.default.invisible,__source:{fileName:_jsxFileName,lineNumber:86}}))))));





}}]);return BalancesScreen;}(_react.default.Component);


var TableRows=function TableRows(props){var
balances=props.balances,navigate=props.navigate;
if(balances){
return balances.map(function(balance){return(
_react.default.createElement(_reactNativePaper.DataTable.Row,{
key:balance.symbol,
style:_balancesScreen.default.dataTableRow,
onPress:function onPress(){return navigate('Actions',{currencySymbol:balance.symbol,currencyName:balance.name});},__source:{fileName:_jsxFileName,lineNumber:99}},

_react.default.createElement(_reactNativePaper.DataTable.Cell,{__source:{fileName:_jsxFileName,lineNumber:104}},_react.default.createElement(_reactNative.Text,{style:_balancesScreen.default.dataTableNameCell,__source:{fileName:_jsxFileName,lineNumber:104}},balance.name)),
_react.default.createElement(_reactNativePaper.DataTable.Cell,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:105}},_react.default.createElement(_reactNative.Text,{style:_balancesScreen.default.dataTableCell,__source:{fileName:_jsxFileName,lineNumber:105}},(0,_filters.numberFormatToShow)(balance.balance))),
_react.default.createElement(_reactNativePaper.DataTable.Cell,{numeric:true,__source:{fileName:_jsxFileName,lineNumber:106}},_react.default.createElement(_reactNative.Text,{style:_balancesScreen.default.dataTableCell,__source:{fileName:_jsxFileName,lineNumber:106}},(0,_filters.numberFormatToShow)((0,_filters.roundNumber)(balance.czkBalance,2),8)))));});


}
return _react.default.createElement(ProgressBar,{balances:balances,__source:{fileName:_jsxFileName,lineNumber:110}});
};

var TotalBalance=function TotalBalance(props){var
balances=props.balances;var
totalCzkBalance=props.totalCzkBalance;
if(balances){
return(
_react.default.createElement(_reactNative.Text,{style:_balancesScreen.default.totalCzkBalance,__source:{fileName:_jsxFileName,lineNumber:118}},"Celkem",

'    ',
(0,_filters.numberFormatToShow)((0,_filters.roundNumber)(totalCzkBalance,2)),
' ',"K\u010D"));



}
return _react.default.createElement(ProgressBar,{balances:balances,__source:{fileName:_jsxFileName,lineNumber:127}});
};

var ProgressBar=function ProgressBar(props){
if(!props.balances){
if(_reactNative.Platform.OS==='ios')return _react.default.createElement(_reactNative.ProgressViewIOS,{__source:{fileName:_jsxFileName,lineNumber:132}});
return _react.default.createElement(_reactNative.ProgressBarAndroid,{__source:{fileName:_jsxFileName,lineNumber:133}});
}
return null;
};

var getErrorsTexts=function getErrorsTexts(errors){
var errorsTexts=[];
errors.forEach(function(error){
var foundExchange=_config.default.exchanges.find(function(exchange){return Object.keys(error)[0].includes(exchange.toLowerCase());});
var subject=foundExchange||'Cryptocompare';
var message=Object.keys(error)[0].includes('Balance')?
'Zůstatek kryptoměn se nepodařilo načíst.':
'Aktuální kurz kryptoměny se nepodařilo načíst.';
message+=' Potažením dolů opakujte pokus.';
errorsTexts.push((0,_defineProperty2.default)({},subject,message));
});
return errorsTexts;
};

var Error=function Error(props){var
errors=props.errors;
if(errors&&errors.length){
return(
_react.default.createElement(_reactNative.View,{style:_infoBar.default.container,__source:{fileName:_jsxFileName,lineNumber:156}},
getErrorsTexts(errors).map(function(error){return(
_react.default.createElement(_InfoBar.default,{
subject:Object.keys(error)[0],
message:Object.values(error)[0],
reason:"error",
key:Math.random().toString(),__source:{fileName:_jsxFileName,lineNumber:158}}));})));




}
return null;
};

var mapStateToProps=function mapStateToProps(state){return{
allBalances:state.allBalancesReducer.allBalances};};


var mapDispatchToProps=function mapDispatchToProps(dispatch){return(0,_redux.bindActionCreators)({onAllBalancesRequest:_allBalances.onAllBalancesRequest},dispatch);};var _default=

(0,_reactRedux.connect)(mapStateToProps,mapDispatchToProps)(BalancesScreen);exports.default=_default;